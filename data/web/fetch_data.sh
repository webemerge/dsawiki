#!/bin/sh

wget \
    --recursive \
    --retry-connrefused \
    --user-agent=Mozilla \
    --tries=3 \
    --timeout=10 \
    --restrict-file-names=unix,nocontrol \
    --wait=1 \
    --random-wait \
    --level=inf \
    --no-clobber \
    --adjust-extension \
    --no-cookies \
    --trust-server-names \
    --rejected-log=./ulisses-regelwiki.de/rejected.log \
    http://ulisses-regelwiki.de/
