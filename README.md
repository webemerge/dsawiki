DSA Regelwiki Parser
=============

## Projekt einrichten

```bash
composer install
yarn build
```

## Daten laden:

```bash
cd data/web
./fetch_data.sh
```

Das Spiegeln des Originalwiki kann mehrere Stunden dauern!

## Daten konvertieren

```bash
bin/console dark-eye:build-file-tree --clean -vv
bin/console dark-eye:parse -vv
```
