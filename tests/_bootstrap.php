<?php

// This is global bootstrap for autoloading

(new \Symfony\Component\Dotenv\Dotenv())->load(dirname(__DIR__).'/.env');

use Codeception\Util\Autoload;

Autoload::addNamespace('Tests\Traits', __DIR__.'/_support/Traits');
Autoload::addNamespace('Tests\Functional', __DIR__.'/functional');
Autoload::addNamespace('Tests\Acceptance', __DIR__.'/acceptance');
Autoload::addNamespace('Tests\Unit', __DIR__.'/unit');
