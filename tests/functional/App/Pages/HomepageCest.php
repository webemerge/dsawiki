<?php

namespace Tests\Functional\App\Pages;

use FunctionalTester;

/**
 * @group example
 */
class HomepageCest
{
    public function _before(FunctionalTester $I)
    {
        // $I->setHost('localhost');
    }

    public function _after(FunctionalTester $I)
    {
    }

    public function homepageLoads(FunctionalTester $I)
    {
        $I->amOnPage('/');
        $I->see('Regelwiki');
    }
}
