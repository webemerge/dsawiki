const $ = require('jquery');
require('datatables.net');
require('datatables.net-bs');

module.exports = function() {
    let $mainTable = $('#main-table');
    let publicationShortcuts = $mainTable.data('shortcuts');
    let attributes = $mainTable.data('attributes');
    let columnSettings = $mainTable.data('columns');

    var applyShortcuts = function (text) {
        if ('undefined' == typeof text) {
            return '';
        }
        text = text.replace(/Seite /g, '');
        $.each(publicationShortcuts, function (searchFor, replaceWith) {
            text = text.replace(new RegExp(searchFor + '(?!")', 'g'), '<abbr title="' + searchFor + '">' + replaceWith + '</abbr>');
        });

        return text;
    };
    var applyAPShortcut = function (text) {
        if ('undefined' == typeof text) {
            return '';
        }
        text = text.replace(/Abenteuerpunkte(?!")/g, '<abbr title="Abenteuerpunkte">AP</abbr>');

        return text;
    };
    var applyAttributes = function (text) {
        if ('undefined' == typeof text) {
            return '';
        }

        text = text.replace(/modifiziert um /, 'mod. ');
        text = text.replace('SK', '<abbr title="Seelenkraft" class="property property-SK">SK</abbr>');
        text = text.replace('ZK', '<abbr title="Zähigkeit" class="property property-ZK">ZK</abbr>');
        $.each(attributes, function (code, name) {
            text = text.replace(new RegExp(code), '<abbr title="' + name + '" class="attribute attribute-' + code + '">' + code + '</abbr>');
        });

        return text;
    };
    var linkify = function (text) {
        if ('undefined' == typeof text) {
            return '';
        }

        return '<a href="' + text + '" target="_blank">' + text + ' <span class="fad fa-external-link"></span></a>';
    };
    var checkify = function (text) {
        if ('undefined' == typeof text) {
            return '';
        }

        if (text) {
            return '<span class="fad fa-check"></span>';
        }

        // force visibility in details list
        return ' ';
    };

    var customColumnDefs = [];
    for (let i = 0; i < columnSettings.length; ++i) {
        if ('data.publication' === columnSettings[i].data) {
            customColumnDefs.push({
                targets: i,
                defaultContent: '',
                render: function (data, type, full, meta) {
                    return applyShortcuts(data);
                }
            });
        }

        if ('data.test_for' === columnSettings[i].data) {
            customColumnDefs.push({
                targets: i,
                defaultContent: '',
                render: function (data, type, full, meta) {
                    return applyAttributes(data);
                }
            });
        }

        if ('data.ap_cost' === columnSettings[i].data) {
            customColumnDefs.push({
                targets: i,
                defaultContent: '',
                render: function (data, type, full, meta) {
                    return applyAPShortcut(data);
                }
            });
        }

        if ('data.wiki_url' === columnSettings[i].data) {
            customColumnDefs.push({
                targets: i,
                defaultContent: '',
                render: function (data, type, full, meta) {
                    return linkify(data);
                }
            });
        }

        if ('data.is_improvised' === columnSettings[i].data) {
            customColumnDefs.push({
                targets: i,
                defaultContent: '',
                render: function (data, type, full, meta) {
                    return checkify(data);
                }
            });
        }
    }

    var table = $mainTable.DataTable({
        data: $mainTable.data('entities'),
        columns: columnSettings,
        scrollX: true,
        scrollCollapse: true,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        order: [[ 1, "asc" ]],
        pageLength: 50,
        stateSave: true,
        columnDefs: customColumnDefs
    });

    var formatSpellExtensions = function (extensions) {
        var innerDetails = '<table class="table table-condensed" cellpadding="3" cellspacing="0" border="0">';
        innerDetails += '<thead><tr><th>Name</th><th>Anforderungen</th><th>Beschreibung</th><th>Publikation</th></tr></thead>';
        innerDetails += '<tbody>';

        $(extensions).each(function (i, extension) {
            var row = '<tr>';
            row += '<td>' + extension.name + '</td>';
            row += '<td>' + extension.requirements + '</td>';
            row += '<td>' + extension.description + '</td>';
            row += '<td>' + applyShortcuts(extension.publication ? extension.publication : '') + '</td>';

            innerDetails += row + '</tr>';
        });
        innerDetails += '</tbody></table>';

        return innerDetails;
    };

    var formatExamples = function (examples) {
        var innerDetails = '<table class="table table-condensed" cellpadding="3" cellspacing="0" border="0">';
        innerDetails += '<thead><tr><th>Name</th><th>Beschreibung</th><th>Publikation</th></tr></thead>';
        innerDetails += '<tbody>';

        $(examples).each(function (i, example) {
            var row = '<tr>';
            row += '<td>' + example.name + '</td>';
            row += '<td>' + example.description + '</td>';
            row += '<td>' + applyShortcuts(example.publication ? example.publication : '') + '</td>';

            innerDetails += row + '</tr>';
        });
        innerDetails += '</tbody></table>';

        return innerDetails;
    };

    var formatDetails = function (rowData, table) {
        var details = $('<table class="table details-table" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;"></table>');

        $(columnSettings).each(function (index, column) {
            if ('details-control' === column.className || !column.title) {
                return;
            }
            var row = $('<tr></tr>');
            var fieldName = column.data.substring(5);

            var value = '';
            if ('undefined' !== typeof rowData.data[fieldName]) {
                value = rowData.data[fieldName];
                switch (fieldName) {
                    case 'spell_extensions':
                    case 'liturgy_extensions':
                    case 'ceremony_extensions':
                        value = formatSpellExtensions(value);
                        break;
                    case 'disadvantage_examples':
                        value = formatExamples(value);
                        break;
                    case 'publication':
                        value = applyShortcuts(value);
                        break;
                    case 'test_for':
                        value = applyAttributes(value);
                        break;
                    case 'wiki_url':
                        value = linkify(value);
                        break;
                    case 'is_improvised':
                        value = checkify(value);
                        break;
                }
            }

            if ('' === value) {
                return;
            }
            row.append($('<th>' + column.title + '</th>'));
            row.append($('<td>' + value + '</td>'));

            details.append(row);
        });

        return details;
    };

    var toggleRow = function (tr) {
        var row = table.row(tr);
        var icon = tr.find('td.details-control').children('svg');

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            icon.removeClass('fa-minus-circle').addClass('fa-plus-circle');
        } else {
            // Open this row
            row.child(formatDetails(row.data(), table)).show();
            icon.removeClass('fa-plus-circle').addClass('fa-minus-circle');
        }
    };

    let $mainTableBody = $('#main-table tbody');
    $mainTableBody.on('click', 'td.details-control', function () {
        let tr = $(this).closest('tr');
        console.log('click!');
        console.log(tr);
        toggleRow(tr);
    });
    $mainTableBody.on('dblclick', 'tr', function () {
        let tr = $(this);
        toggleRow(tr);

        return false;
    });
};
