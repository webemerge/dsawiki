<?php

/*
 * =================================================================
 * Start CI auto fetch (downloading robo dependencies automatically)
 * =================================================================.
 */
/*
 * =================================================================
 * Start CI auto fetch (downloading robo dependencies automatically)
 * =================================================================.
 */
(is_dir('.ci') || mkdir('.ci')) && chdir('.ci');
if (!is_file('composer.json')) {
    exec('composer req c33s-toolkit/robo-file -n', $output, $resultCode);
    if ($resultCode > 0) {
        copy('https://getcomposer.org/composer.phar', 'composer');
        exec('php composer req c33s-toolkit/robo-file -n');
        unlink('composer');
    }
} else {
    exec('composer install --dry-run -n 2>&1', $output);
    if (false === in_array('Nothing to install or update', $output)) {
        fwrite(STDERR, "\n##### Updating .ci dependencies #####\n\n") && exec('composer install -n');
    }
}
chdir('..');
require '.ci/vendor/autoload.php';
/*
 * =================================================================
 *                        End CI auto fetch
 * =================================================================.
 */

/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
class RoboFile extends \Robo\Tasks
{
    use \C33s\Robo\C33sTasks;

    protected $portsToCheck = [
        // 'http' => null,
        // 'https' => null,
        // 'mysql' => null,
        // 'postgres' => null,
        // 'elasticsearch' => null,
        // 'mongodb' => null,
    ];

    /**
     * @hook pre-command
     */
    public function preCommand()
    {
        $this->stopOnFail(true);
        $this->_prepareCiModules([
            'composer' => '@latest',
            'php-cs-fixer' => 'v2.15.0',
        ]);
    }

    /**
     * Initialize project.
     */
    public function init()
    {
        if (!$this->confirmIfInteractive('Have you read the README.md?')) {
            $this->abort();
        }

        if (!$this->ciCheckPorts($this->portsToCheck)) {
            if (!$this->confirmIfInteractive('Do you want to continue?')) {
                $this->abort();
            }
        }

        $this->composerGlobalRequire('fxp/composer-asset-plugin', '~1.3');
        $this->composerGlobalRequire('hirak/prestissimo', '^0.3');

        $this->update();
    }

    /**
     * Perform code-style checks.
     *
     * @param string $arguments Optional path or other arguments
     */
    public function check($arguments = '')
    {
        $this->_execPhp("php .ci/bin/php-cs-fixer.phar fix --verbose --dry-run $arguments");
    }

    /**
     * Perform code-style checks and cleanup source code automatically.
     *
     * @param string $arguments Optional path or other arguments
     */
    public function fix($arguments = '')
    {
        if ($this->confirmIfInteractive('Do you really want to run php-cs-fixer on your source code?')) {
            $this->_execPhp("php .ci/bin/php-cs-fixer.phar fix --verbose $arguments");
        } else {
            $this->abort();
        }
    }

    /**
     * Update the Project.
     */
    public function update()
    {
        if ($this->isEnvironmentCi()) {
            $this->_execPhp('php ./.ci/bin/composer.phar install --no-progress --no-suggest --prefer-dist --optimize-autoloader');
        } else {
            $this->_execPhp('php ./.ci/bin/composer.phar install');
        }
        $this->assetsBuild();
    }

    /**
     * Build yarn dev assets.
     */
    public function assetsBuild(): void
    {
        $this->_exec('yarn dev');
    }

    /**
     * Build/Compile Encore assets.
     */
    public function assetsInstall(): void
    {
        $environment = 'dev';
        if ($this->isEnvironmentProduction()) {
            $environment = 'production';
        }
        $this->_exec("yarn run encore $environment");
    }

    /**
     * Start Encore Assets Server (not for production server!).
     */
    public function assetsServer(): void
    {
        if ($this->isEnvironmentProduction()) {
            if (!$this->confirm('YOU MUST NOT USE THIS ON THE PRODUCTION SERVER! CONTINUE?')) {
                $this->abort();
            }
        }
        $this->_exec('yarn dev-server');
    }

    /**
     * Run tests.
     */
    public function test($suite = '')
    {
        if ($this->isEnvironmentCi()) {
            $this->_execPhp("php ./vendor/codeception/codeception/codecept run $suite --coverage-xml --coverage-html --coverage-text", true);
            $this->outputCoverage();
        } else {
            $this->_execPhp("php ./vendor/codeception/codeception/codecept run $suite");
        }
    }

    /**
     * Run Unit tests.
     */
    public function testUnit()
    {
        $this->test('unit');
    }

    /**
     * Run Functional tests.
     */
    public function testFunctional()
    {
        $this->test('functional');
    }

    /**
     * Write plain coverage line used for gitlab CI detecting the coverage score.
     */
    private function outputCoverage(): void
    {
        $this->writeln(file(__DIR__.'/tests/_output/coverage.txt')[8]);
    }

    /**
     * Clear and warmup symfony caches.
     */
    public function cc()
    {
        $this->_execPhp('bin/console cache:clear --no-warmup');
        $this->_execPhp('bin/console cache:warmup');
    }

    /**
     * Reset database and import fixtures.
     */
    public function reset()
    {
        if ($this->isEnvironmentProduction()) {
            $this->abort('DO NOT RUN THIS IN PRODUCTION');
        }

        if (!$this->isEnvironmentCi()) {
            // $this->_execPhp('php bin/console doctrine:database:create --if-not-exists');
        }
        // $this->_execPhp('php bin/console doctrine:schema:drop --force');
        // $this->_execPhp('php bin/console doctrine:schema:create');
        // $this->_execPhp('php bin/console doctrine:fixtures:load --no-interaction');
    }

    public function createStaticDump(): void
    {
        copy('.env.local', '.env.local__bak');
        copy('.env.local__prod', '.env.local');
        $this->_cleanDir('dsawiki.local.dev');
        $this->_exec('yarn build');
        $this->_execPhp('bin/console cache:clear --env=prod');
        $this->_exec('wget \
            --recursive \
            --page-requisites \
            --convert-links \
            --no-parent \
            --restrict-file-names=windows,unix,nocontrol \
            --level=inf \
            --no-clobber \
            --adjust-extension \
            --no-cookies \
            --trust-server-names \
            https://dsawiki.local.dev/'
        );
        copy('.env.local__bak', '.env.local');
    }

    public function build(): void
    {
        $this->createStaticDump();
        $baseName = 'dsawiki-hesinde-edition';
        $filename = $baseName.'-'.date('Y-m-d').'.zip';
        if (file_exists($filename)) {
            unlink($filename);
        }
        $this->taskPack($filename)
            ->addFile("$baseName/", 'dsawiki.local.dev/')
            ->run()
        ;
    }

    /**
     * Check if the current Environment is CI.
     */
    protected function isEnvironmentCi(): bool
    {
        if ('true' === getenv('CI') || '1' === getenv('CI')) {
            return true;
        }
        if (getenv('CI_BUILD_TOKEN')) {
            return true;
        }
        if (getenv('CI_JOB_ID')) {
            return true;
        }

        return false;
    }

    /**
     * Check if the current Environment is production.
     */
    protected function isEnvironmentProduction(): bool
    {
        if ('dev' === strtolower(getenv('ENVIRONMENT'))) {
            return false;
        }
        if ('development' === strtolower(getenv('ENVIRONMENT'))) {
            return true;
        }
        if ($this->isEnvironmentCi()) {
            return false;
        }

        return true;
    }

    /**
     * Check if the current Environment is Development.
     */
    protected function isEnvironmentDevelopment(): bool
    {
        return !$this->isEnvironmentProduction();
    }
}
