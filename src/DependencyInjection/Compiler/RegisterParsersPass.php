<?php

namespace App\DependencyInjection\Compiler;

use DarkEye\Parser\ModelParser;
use DarkEye\Parser\PurifierAware;
use DarkEye\Parser\SlugifyAware;
use DarkEye\Parser\WikiParser;
use Psr\Log\LoggerAwareInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

final class RegisterParsersPass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     */
    public function process(ContainerBuilder $container)
    {
        $this->registerModelParsers($container);
    }

    private function registerModelParsers(ContainerBuilder $container)
    {
        $parserDefinition = $container->getDefinition(WikiParser::class);

        $dir = $container->getParameter('kernel.project_dir').'/lib/DarkEye/Parser/Model';
        $files = new Finder();

        $files
            ->in($dir)
            ->name('*.php')
        ;

        $purifier = $container->getDefinition('exercise_html_purifier.default');
        $slugify = $container->getDefinition('cocur_slugify');
        $logger = $container->getDefinition('logger');
        foreach ($files as $file) {
            /* @var $file SplFileInfo */
            $modelName = $file->getBasename('.php');
            $id = 'dark_eye.model_parser.'.strtolower(str_replace('\\', '.', $modelName));
            if ($container->hasDefinition($id)) {
                continue;
            }

            try {
                $className = "DarkEye\\Parser\\Model\\{$modelName}";
                $ref = new \ReflectionClass($className);
                if ($ref->implementsInterface(ModelParser::class) && !$ref->isAbstract()) {
                    $modelDefinition = new Definition($className);
                    $modelDefinition->setAutowired(true);
                    $modelDefinition->addTag('dark_eye.model_parser');
                    if ($ref->implementsInterface(PurifierAware::class)) {
                        $modelDefinition->addMethodCall('setPurifier', [$purifier]);
                    }
                    if ($ref->implementsInterface(SlugifyAware::class)) {
                        $modelDefinition->addMethodCall('setSlugify', [$slugify]);
                    }
                    if ($ref->implementsInterface(LoggerAwareInterface::class)) {
                        $modelDefinition->addMethodCall('setLogger', [$logger]);
                    }

                    $container->setDefinition($id, $modelDefinition);
                    $parserDefinition->addMethodCall('addModelParser', [$modelDefinition]);
                }
            } catch (\ReflectionException $e) {
                // class does not exist
            }
        }
    }
}
