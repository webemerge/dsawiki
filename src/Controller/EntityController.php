<?php

namespace App\Controller;

use Cocur\Slugify\SlugifyInterface;
use DarkEye\Entity\Entity;
use DarkEye\Entity\EntityRepository;
use DarkEye\Parser\ColumnDefinition;
use DarkEye\Schema\SchemaProvider;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

final class EntityController extends AbstractController
{
    /**
     * @var SchemaProvider
     */
    private $schemaProvider;

    /**
     * @var EntityRepository
     */
    private $entityRepository;

    /**
     * @var SlugifyInterface
     */
    private $slugify;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(
        SchemaProvider $schemaProvider,
        EntityRepository $entityRepository,
        SlugifyInterface $slugify,
        TranslatorInterface $translator
    ) {
        $this->schemaProvider = $schemaProvider;
        $this->entityRepository = $entityRepository;
        $this->slugify = $slugify;
        $this->translator = $translator;
    }

    /**
     * @ Route("/{alias}", name="entity_table")
     * @ Route("/{alias}/{filterBy}/{filterValue}", name="entity_table")
     */
    public function table(Request $request = null): Response
    {
        if (null === $request) {
            // this is the fake request used by simplify/symfony-static-dumper
            $request = $this->get('request_stack')->getCurrentRequest();
            [, $alias, $filterBy, $filterValue] = explode('/', $request->get('_route'));
        } else {
            $alias = $request->get('alias');
            $filterBy = $request->get('filterBy');
            $filterValue = $request->get('filterValue');
        }

        $schema = $this->schemaProvider->getSchema($alias);
        if (null === $schema) {
            throw $this->createNotFoundException();
        }

        $entities = $this->entityRepository->find($schema->getName());
        $filterBy = str_replace('-', '_', $filterBy);

        if (isset($filterBy) && isset($filterValue)) {
            $entities = $this->filterEntities($filterBy, $filterValue, $entities);
        }

        $entities = array_values($entities);

        $columnDefinition = new ColumnDefinition();
        $columns = $columnDefinition->getColumnSettingsForSchema($schema, $this->translator);

        return $this->render('entity/table.html.twig', [
            'entities' => $entities,
            'schema' => $schema,
            'columns' => $columns,
            'publicationShortcuts' => $columnDefinition->getPublicationShortcuts(),
            'attributes' => $columnDefinition->getAttributes($this->translator),
        ]);
    }

    private function filterEntities(string $filterBy, $filterValue, array $entities): array
    {
        return array_filter($entities, function (Entity $entity) use ($filterBy, $filterValue) {
            return
                isset($entity->data[$filterBy]) &&
                $this->slugify->slugify($entity->data[$filterBy]) === $filterValue
            ;
        });
    }
}
