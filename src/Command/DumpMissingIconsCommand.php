<?php

namespace App\Command;

use DarkEye\Entity\EntityRepository;
use DarkEye\Schema\AbstractSchema;
use DarkEye\Schema\SchemaProvider;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class DumpMissingIconsCommand extends Command implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    protected static $defaultName = 'dark-eye:dump-missing-icons';

    /**
     * @var EntityRepository
     */
    private $entityRepository;

    /**
     * @var SchemaProvider
     */
    private $schemaProvider;

    public function __construct(EntityRepository $entityRepository, SchemaProvider $schemaProvider)
    {
        $this->entityRepository = $entityRepository;
        $this->schemaProvider = $schemaProvider;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Dump missing sub types and icons for schemas supporting sub types');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $missing = 0;
        foreach ($this->schemaProvider->getAll() as $schema) {
            if (!$schema->getSubSchemaFieldName()) {
                continue;
            }

            $missing += $this->checkSchema($schema, $output);
        }

        return $missing;
    }

    private function checkSchema(AbstractSchema $schema, OutputInterface $output): int
    {
        $this->logger->info('Checking schema '.$schema->getName());
        $entities = $this->entityRepository->find($schema->getName());
        $fieldName = $schema->getSubSchemaFieldName();
        $existingIcons = $schema->getSubSchemaIcons();
        $missing = [];
        foreach ($entities as $entity) {
            if (!isset($entity->data[$fieldName])) {
                continue;
            }
            $value = $entity->data[$fieldName];
            if (!isset($existingIcons[$value]) && !isset($missing[$value])) {
                $missing[$value] = $value;
            }
        }

        if (count($missing) > 0) {
            $output->writeln($schema->getName().':');
            foreach ($missing as $value) {
                $output->writeln("    '{$value}' => 'fa-',");
            }
        }

        return count($missing);
    }
}
