<?php

namespace App\Command;

use DarkEye\Parser\ModelParser;
use DarkEye\Parser\WikiParser;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

final class ParseWikiCommand extends Command
{
    protected static $defaultName = 'dark-eye:parse';

    private $parser;

    public function __construct(WikiParser $parser)
    {
        $this->parser = $parser;
        parent::__construct();
    }

    /**
     * Configures the current command.
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Parse previously built file tree into actual objects.')
            ->addArgument(
                'model',
                InputArgument::OPTIONAL,
                'restrict to the given model',
                null
            )
            ->addOption(
                'list-models',
                null,
                InputOption::VALUE_NONE,
                'List all available models'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if ($input->getOption('list-models')) {
            $this->listModels($this->parser->getModelParsers(), $output);

            return 0;
        }

        $this->parser->cleanData();

        if ($input->hasArgument('model') && $input->getArgument('model')) {
            $this->parser->parseModel('DarkEye\\Schema\\'.$input->getArgument('model'));
        } else {
            $this->parser->parseAll();
        }

        return 0;
    }

    /**
     * @param ModelParser[] $modelParsers
     */
    private function listModels(array $modelParsers, OutputInterface $output): void
    {
        $output->writeln('Available models:');
        foreach ($modelParsers as $modelParser) {
            $output->writeln($modelParser->getModelClass());
        }
    }
}
