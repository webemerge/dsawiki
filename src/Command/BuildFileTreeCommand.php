<?php

namespace App\Command;

use DarkEye\FileTree\FileTreeBuilder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

final class BuildFileTreeCommand extends Command
{
    protected static $defaultName = 'dark-eye:build-file-tree';

    private $fileTreeBuilder;

    public function __construct(FileTreeBuilder $fileTreeBuilder)
    {
        parent::__construct();

        $this->fileTreeBuilder = $fileTreeBuilder;
    }

    /**
     * Configures the current command.
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Convert raw wget HTML data into parsable tree structure based on menu.')
            ->addOption(
                'clean',
                null,
                InputOption::VALUE_NONE,
                'Clean tree directory before import'
            )
            ->addArgument(
                'name-pattern',
                InputArgument::OPTIONAL,
                'Optional name pattern to match specific files',
                null
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if ($input->getOption('clean')) {
            $this->fileTreeBuilder->removeTree();
        }

        if (!$input->hasArgument('name-pattern')) {
            $this->fileTreeBuilder->buildTree();
        } else {
            $this->fileTreeBuilder->buildTree($input->getArgument('name-pattern'));
        }

        return 0;
    }
}
