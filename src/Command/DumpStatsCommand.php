<?php

namespace App\Command;

use App\Menu\GroupItem;
use App\Menu\MenuBuilder;
use App\Menu\MenuItem;
use App\Menu\SchemaItem;
use DarkEye\Entity\EntityRepository;
use DarkEye\Schema\AbstractSchema;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use function Symfony\Component\String\u;

final class DumpStatsCommand extends Command
{
    protected static $defaultName = 'dark-eye:dump-stats';

    /**
     * @var EntityRepository
     */
    private $entityRepository;

    /**
     * @var MenuBuilder
     */
    private $menuBuilder;

    public function __construct(EntityRepository $entityRepository, MenuBuilder $menuBuilder)
    {
        $this->entityRepository = $entityRepository;
        $this->menuBuilder = $menuBuilder;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Dump content statistics to use for Scriptorium Aventuris')
            ->addOption('html', null, InputOption::VALUE_NONE, 'Dump as HTML list')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $menu = $this->menuBuilder->buildMainMenu();

        if ($input->getOption('html')) {
            $this->dumpAsHtml($menu, $output);
        } else {
            $this->dumpAsList($menu, $output);
        }

        return 0;
    }

    private function dumpAsHtml(GroupItem $menu, OutputInterface $output): void
    {
        $output->writeln('<ul>');
        foreach ($menu->getChildren() as $item) {
            $this->dumpItemAsHtml($item, $output, 1);
        }
        $output->writeln('</ul>');
    }

    private function dumpItemAsHtml(MenuItem $item, OutputInterface $output, int $level): void
    {
        $indent = u('  ')->repeat($level);
        if ($item instanceof GroupItem) {
            $output->writeln($indent->append('<li>'));

            $indent = u('  ')->repeat(++$level);

            $output->writeln($indent->append('<ul>'));
            foreach ($item->getChildren() as $childItem) {
                $this->dumpItemAsHtml($childItem, $output, $level + 1);
            }
            $output->writeln($indent->append('</ul>'));

            $indent = u('  ')->repeat(--$level);

            $output->writeln($indent->append('</li>'));
        } elseif ($item instanceof SchemaItem) {
            $schema = $item->getSchema();
            $entities = $this->entityRepository->find($schema->getName());
            $line = $indent->append('<li>'.$schema->getTranslatedName().': '.count($entities));

            $subTypes = $this->getNumberOfSubCategories($schema);
            if (null !== $subTypes) {
                $line = $line->append(" in {$subTypes} Kategorien");
            }
            $line = $line->append('</li>');
            $output->writeln($line);
        }
    }

    private function dumpAsList(GroupItem $menu, OutputInterface $output): void
    {
        foreach ($menu->getChildren() as $item) {
            $this->dumpItemAsList($item, $output, 0);
        }
    }

    private function dumpItemAsList(MenuItem $item, OutputInterface $output, int $level): void
    {
        $line = u('  ')->repeat($level)->append('- ');
        if ($item instanceof GroupItem) {
            $output->writeln($line->append($item->getLabel()));
            foreach ($item->getChildren() as $childItem) {
                $this->dumpItemAsList($childItem, $output, $level + 1);
            }
        } elseif ($item instanceof SchemaItem) {
            $schema = $item->getSchema();
            $entities = $this->entityRepository->find($schema->getName());
            $line = $line->append($schema->getTranslatedName().': '.count($entities));

            $subTypes = $this->getNumberOfSubCategories($schema);
            if (null !== $subTypes) {
                $line = $line->append(" in {$subTypes} Kategorien");
            }
            $output->writeln($line);
        }
    }

    private function getNumberOfSubCategories(AbstractSchema $schema): ?int
    {
        if (null === $schema->getSubSchemaFieldName()) {
            return null;
        }

        $entities = $this->entityRepository->find($schema->getName());
        $fieldName = $schema->getSubSchemaFieldName();
        $subTypes = [];
        foreach ($entities as $entity) {
            if (!isset($entity->data[$fieldName])) {
                continue;
            }
            $value = $entity->data[$fieldName];
            if (!isset($subTypes[$value])) {
                $subTypes[$value] = $value;
            }
        }

        return count($subTypes);
    }
}
