<?php

namespace App\Menu;

use DarkEye\Schema\AbstractSchema;

final class SchemaItem implements MenuItem
{
    /**
     * @var AbstractSchema
     */
    private $schema;

    public function __construct(AbstractSchema $schema)
    {
        $this->schema = $schema;
    }

    public function getSchema(): AbstractSchema
    {
        return $this->schema;
    }
}
