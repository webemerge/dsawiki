<?php

namespace App\Menu;

use DarkEye\Schema\Advantage;
use DarkEye\Schema\Armor;
use DarkEye\Schema\Blessing;
use DarkEye\Schema\Ceremony;
use DarkEye\Schema\Creature;
use DarkEye\Schema\Disadvantage;
use DarkEye\Schema\Language;
use DarkEye\Schema\Liturgy;
use DarkEye\Schema\MagicSpell;
use DarkEye\Schema\MagicTrick;
use DarkEye\Schema\Plant;
use DarkEye\Schema\Ritual;
use DarkEye\Schema\SchemaProvider;
use DarkEye\Schema\Skill;
use DarkEye\Schema\TradeSecret;
use DarkEye\Schema\Weapon;

final class MenuBuilder
{
    /**
     * @var SchemaProvider
     */
    private $schemaProvider;

    public function __construct(SchemaProvider $schemaProvider)
    {
        $this->schemaProvider = $schemaProvider;
    }

    public function buildMainMenu(): GroupItem
    {
        $menu = new GroupItem('main menu', '');

        $menu->addChild(new GroupItem('Magie', 'fa-magic', [
            new SchemaItem($this->schemaProvider->getSchema(MagicSpell::class)),
            new SchemaItem($this->schemaProvider->getSchema(MagicTrick::class)),
            new SchemaItem($this->schemaProvider->getSchema(Ritual::class)),
        ]));

        $menu->addChild(new GroupItem('Götterwirken', 'fa-university', [
            new SchemaItem($this->schemaProvider->getSchema(Liturgy::class)),
            new SchemaItem($this->schemaProvider->getSchema(Blessing::class)),
            new SchemaItem($this->schemaProvider->getSchema(Ceremony::class)),
        ]));

        $menu->addChild(new GroupItem('Vor- und Nachteile', 'fa-check-square', [
            new SchemaItem($this->schemaProvider->getSchema(Advantage::class)),
            new SchemaItem($this->schemaProvider->getSchema(Disadvantage::class)),
        ]));

        $menu->addChild(new GroupItem('Fertigkeiten', 'fa-user', [
            new SchemaItem($this->schemaProvider->getSchema(Skill::class)),
            new SchemaItem($this->schemaProvider->getSchema(TradeSecret::class)),
            new SchemaItem($this->schemaProvider->getSchema(Language::class)),
        ]));

        $menu->addChild(new SchemaItem($this->schemaProvider->getSchema(Creature::class)));
        $menu->addChild(new SchemaItem($this->schemaProvider->getSchema(Plant::class)));

        $menu->addChild(new GroupItem('Ausrüstung', 'fa-box-open', [
            new SchemaItem($this->schemaProvider->getSchema(Armor::class)),
            new SchemaItem($this->schemaProvider->getSchema(Weapon::class)),
        ]));

        return $menu;
    }
}
