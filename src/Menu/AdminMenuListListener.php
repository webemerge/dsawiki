<?php

namespace App\Menu;

use Cocur\Slugify\SlugifyInterface;
use DarkEye\Schema\AbstractSchema;
use DarkEye\Schema\SchemaProvider;
use KevinPapst\AdminLTEBundle\Event\SidebarMenuEvent;
use KevinPapst\AdminLTEBundle\Model\MenuItemModel;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;

final class AdminMenuListListener implements EventSubscriberInterface
{
    private const ICON_PREFIX = 'fad fa-fw ';

    /**
     * @var SchemaProvider
     */
    private $schemaProvider;

    /**
     * @var SlugifyInterface
     */
    private $slugify;
    /**
     * @var MenuBuilder
     */
    private $menuBuilder;

    public function __construct(SchemaProvider $schemaProvider, SlugifyInterface $slugify, MenuBuilder $menuBuilder)
    {
        $this->schemaProvider = $schemaProvider;
        $this->slugify = $slugify;
        $this->menuBuilder = $menuBuilder;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            SidebarMenuEvent::class => ['onSetupMenu', 100],
        ];
    }

    public function onSetupMenu(SidebarMenuEvent $event)
    {
        $request = $event->getRequest();

        foreach ($this->getMenu($request) as $item) {
            $event->addItem($item);
        }
    }

    /**
     * @return MenuItemModel[]
     */
    private function getMenu(Request $request)
    {
        $menu = $this->menuDefinitionToItem($this->menuBuilder->buildMainMenu());

        return $this->activateByRoute(
            $request->get('_route'),
            [
                'alias' => $request->get('alias'),
                'filterBy' => $request->get('filterBy'),
                'filterValue' => $request->get('filterValue'),
            ],
            $menu->getChildren()
        );
    }

    /**
     * @param MenuItemModel[] $items
     *
     * @return MenuItemModel[]
     */
    private function activateByRoute(string $routeName, array $routeArgs, array $items): array
    {
        foreach ($items as $item) {
            if ($item->hasChildren()) {
                $this->activateByRoute($routeName, $routeArgs, $item->getChildren());
            } elseif ($item->getRoute() === $routeName && $item->getRouteArgs() == $routeArgs) {
                $item->setIsActive(true);
            }
        }

        return $items;
    }

    private function addEntityItem(
        string $menuLabel,
        string $schemaAlias,
        string $icon,
        MenuItemModel $parentItem,
        ?string $filterBy = null,
        ?string $filterValue = null
    ): MenuItemModel {
        $routeName = "/{$schemaAlias}";
        if (null !== $filterBy) {
            $routeName .= "/{$filterBy}/{$filterValue}";
        }

        $item = new MenuItemModel(
            $schemaAlias,
            $menuLabel,
            $routeName,
            [
                'alias' => $schemaAlias,
                'filterBy' => $filterBy,
                'filterValue' => $filterValue,
            ],
            $icon
        );
        $parentItem->addChild($item);

        return $item;
    }

    private function addGroupItem(MenuItemModel $parentItem, string $menuLabel, string $icon): MenuItemModel
    {
        $item = new MenuItemModel(
            $this->slugify->slugify($menuLabel),
            $menuLabel,
            null,
            [],
            self::ICON_PREFIX.$icon
        );
        $parentItem->addChild($item);

        return $item;
    }

    private function addSchemaItems(MenuItemModel $parentItem, AbstractSchema $schema): void
    {
        if (null === $schema->getSubSchemaFieldName()) {
            $this->addEntityItem($schema->getTranslatedName(), $schema->getAlias(), 'fa-fw '.$schema->getIcon(), $parentItem);

            return;
        }

        $filterFieldName = str_replace('_', '-', $schema->getSubSchemaFieldName());
        $subParent = $this->addEntityItem($schema->getTranslatedName(), $schema->getAlias(), 'fa-fw '.$schema->getIcon(), $parentItem);
        $this->addEntityItem('schema.all', $schema->getAlias(), 'fa-fw '.$schema->getIcon(), $subParent);

        foreach ($schema->getSubSchemaIcons() as $filterValue => $icon) {
            $filterSlug = $this->slugify->slugify($filterValue);
            $this->addEntityItem($filterValue, $schema->getAlias(), 'fa-fw fad '.$icon, $subParent, $filterFieldName, $filterSlug);
        }
    }

    private function menuDefinitionToItem(GroupItem $group, ?MenuItemModel $parentItem = null): MenuItemModel
    {
        if (null === $parentItem) {
            $parentItem = new MenuItemModel('menu', '', null);
        }

        foreach ($group->getChildren() as $childDefinition) {
            if ($childDefinition instanceof GroupItem) {
                $item = $this->addGroupItem($parentItem, $childDefinition->getLabel(), $childDefinition->getIcon());
                $this->menuDefinitionToItem($childDefinition, $item);
            } elseif ($childDefinition instanceof SchemaItem) {
                $this->addSchemaItems($parentItem, $childDefinition->getSchema());
            }
        }

        return $parentItem;
    }
}
