<?php

namespace App\Menu;

final class GroupItem implements MenuItem
{
    /**
     * @var string
     */
    private $label;

    /**
     * @var string
     */
    private $icon;

    /**
     * @var MenuItem[]
     */
    private $children;

    public function __construct(string $label, string $icon, array $children = [])
    {
        $this->label = $label;
        $this->icon = $icon;
        $this->children = $children;
    }

    public function addChild(MenuItem $item): void
    {
        $this->children[] = $item;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getIcon(): string
    {
        return $this->icon;
    }

    /**
     * @return MenuItem[]|array
     */
    public function getChildren(): array
    {
        return $this->children;
    }
}
