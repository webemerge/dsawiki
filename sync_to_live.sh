#! /bin/sh

REMOTE_DIR="/var/www/dsawiki.vworld.at/private"

rsync -av --del /srv/www/vhosts/dsawiki.local/ h1-ic1.h1.web-emerge.com:$REMOTE_DIR/live/ \
    --exclude "/var/cache/" --exclude "/var/log/" --exclude "/data/web/*" --exclude "/data/file-tree/*" --exclude "/tests/_*" --exclude /.idea --exclude .git \
    --exclude /.ci/bin --exclude /.ci/cache --exclude /.ci/vendor \
    --exclude /public/bundles --exclude /public/build --exclude /.env.local \
    --exclude /node_modules \
    --exclude /web/stats --exclude /vendor

ssh h1-ic1.h1.web-emerge.com "cd $REMOTE_DIR/live/; composer.phar install --no-interaction; yarn build;"
ssh h1-ic1.h1.web-emerge.com "$REMOTE_DIR/fix_permissions.sh"
