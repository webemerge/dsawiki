<?php

namespace DarkEye\Routing;

use App\Controller\EntityController;
use Cocur\Slugify\SlugifyInterface;
use DarkEye\Schema\SchemaProvider;
use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

final class SchemaRouteLoader extends Loader
{
    /**
     * @var bool
     */
    private $isLoaded = false;

    /**
     * @var SchemaProvider
     */
    private $schemaProvider;

    /**
     * @var SlugifyInterface
     */
    private $slugify;

    public function __construct(SchemaProvider $schemaProvider, SlugifyInterface $slugify)
    {
        $this->schemaProvider = $schemaProvider;
        $this->slugify = $slugify;
    }

    public function load($resource, $type = null)
    {
        if ($this->isLoaded) {
            throw new \RuntimeException('Do not add the "dark_eye_schema" loader twice');
        }

        $routes = new RouteCollection();
        foreach ($this->schemaProvider->getAll() as $schema) {
            $path = '/'.$schema->getAlias();
            $defaults = [
                '_controller' => EntityController::class.'::table',
                'alias' => $schema->getAlias(),
            ];
            $routes->add($path, new Route($path, $defaults));

            $subFieldName = $schema->getSubSchemaFieldName();
            if (null !== $subFieldName) {
                $fieldName = $this->slugify->slugify($subFieldName);
                foreach ($schema->getSubSchemaIcons() as $filterValue => $icon) {
                    $value = $this->slugify->slugify($filterValue);
                    $path = "/{$schema->getAlias()}/{$fieldName}/{$value}";
                    $defaults = [
                        '_controller' => EntityController::class.'::table',
                        'alias' => $schema->getAlias(),
                        'filterBy' => $fieldName,
                        'filterValue' => $value,
                    ];

                    $routes->add($path, new Route($path, $defaults));
                }
            }
        }
        $this->isLoaded = true;

        return $routes;
    }

    public function supports($resource, $type = null)
    {
        return 'dark_eye_schema' === $type;
    }
}
