<?php

namespace DarkEye\Parser;

use DarkEye\Entity\Entity;
use Symfony\Component\Serializer\Serializer;

interface ModelParser
{
    /**
     * @return string
     */
    public function getModelClass();

    /**
     * @param string $sourceDir
     *
     * @return Entity[]
     */
    public function parse($sourceDir, Serializer $serializer);
}
