<?php

namespace DarkEye\Parser\PageProvider;

use DarkEye\Parser\Content\Page;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Serializer\Serializer;

final class DirectoryPageProvider implements PageProvider
{
    /**
     * @var string
     */
    private $treeDir;

    /**
     * @var array
     */
    private $directories;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @param string $treeDir
     */
    public function __construct($treeDir, array $directories, Serializer $serializer)
    {
        $this->treeDir = $treeDir;
        $this->directories = $directories;
        $this->serializer = $serializer;
    }

    /**
     * Get an array of pages.
     *
     * @return Page[]
     */
    public function findPages()
    {
        $pages = [];

        foreach ($this->directories as $baseDir) {
            $finder = Finder::create()
                ->directories()
                ->depth(0)
                ->sortByName()
                ->in(realpath($this->treeDir.'/'.$baseDir))
            ;

            foreach ($finder as $dir) {
                /* @var $dir SplFileInfo */
                $page = $this->parsePage($dir);
                $pages[$page->getPath()] = $page;
            }
        }

        return $pages;
    }

    private function parsePage(SplFileInfo $directory): Page
    {
        $filename = $directory->getFilename();
        $content = file_get_contents($directory->getPathname().'/raw.'.$filename.'-page.json');

        return $this->serializer->deserialize($content, Page::class, 'json');
    }
}
