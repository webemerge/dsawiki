<?php

namespace DarkEye\Parser\PageProvider;

use DarkEye\Parser\Content\Page;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Serializer\Serializer;

/**
 * Find all pages at the end nodes of any sub directories.
 */
final class NestedDirectoryParser implements PageProvider
{
    /**
     * @var string
     */
    private $treeDir;

    /**
     * @var array
     */
    private $directories;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var bool
     */
    private $includeParentDir;

    public function __construct(
        string $treeDir,
        array $directories,
        Serializer $serializer,
        bool $includeParentDir = false
    ) {
        $this->treeDir = $treeDir;
        $this->directories = $directories;
        $this->serializer = $serializer;
        $this->includeParentDir = $includeParentDir;
    }

    /**
     * Get an array of pages.
     *
     * @return Page[]
     */
    public function findPages()
    {
        $pages = [];

        foreach ($this->directories as $baseDir) {
            $finder = Finder::create()
                ->directories()
                ->sortByName()
                ->in(realpath($this->treeDir.'/'.$baseDir))
            ;

            foreach ($finder as $dir) {
                /* @var $dir SplFileInfo */
                $subDirs = glob($dir->getPathname().'/*', GLOB_ONLYDIR);
                if (!$this->includeParentDir && count($subDirs) > 0) {
                    continue;
                }

                $page = $this->parsePage($dir);
                $pages[$page->getPath()] = $page;
            }
        }

        return $pages;
    }

    private function parsePage(SplFileInfo $directory): Page
    {
        $filename = $directory->getFilename();
        $content = file_get_contents($directory->getPathname().'/raw.'.$filename.'-page.json');

        return $this->serializer->deserialize($content, Page::class, 'json');
    }
}
