<?php

namespace DarkEye\Parser\PageProvider;

use DarkEye\Parser\Content\Page;

interface PageProvider
{
    /**
     * Get an array of pages.
     *
     * @return Page[]
     */
    public function findPages();
}
