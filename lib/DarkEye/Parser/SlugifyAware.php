<?php

namespace DarkEye\Parser;

use Cocur\Slugify\SlugifyInterface;

interface SlugifyAware
{
    public function setSlugify(SlugifyInterface $slugify);
}
