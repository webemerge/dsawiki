<?php

namespace DarkEye\Parser;

use DarkEye\Exception\ParserException;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\SerializerInterface;

final class WikiParser implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var string
     */
    private $dataDir;

    /**
     * @var string
     */
    private $sourceDir;

    /**
     * @var ModelParser[]
     */
    private $modelParsers = [];

    /**
     * @var string
     */
    private $treeDir;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(string $sourceDir, string $dataDir, string $treeDir, SerializerInterface $serializer)
    {
        $this->sourceDir = $sourceDir;
        $this->dataDir = $dataDir;
        $this->treeDir = $treeDir;
        $this->serializer = $serializer;
    }

    public function addModelParser(ModelParser $modelParser)
    {
        $this->modelParsers[$modelParser->getModelClass()] = $modelParser;
        ksort($this->modelParsers);
    }

    /**
     * @return ModelParser[]
     */
    public function getModelParsers()
    {
        return $this->modelParsers;
    }

    /**
     * Clean all existing parsed json data.
     */
    public function cleanData()
    {
        $fs = new Filesystem();
        if ($fs->exists($this->dataDir)) {
            $fs->remove($this->dataDir);
        }

        $fs->mkdir($this->dataDir);
    }

    /**
     * Parse all available models.
     */
    public function parseAll()
    {
        foreach (array_keys($this->modelParsers) as $className) {
            $this->parseModel($className);
        }
    }

    /**
     * @param string $className
     */
    public function parseModel($className)
    {
        if (!isset($this->modelParsers[$className])) {
            throw ParserException::modelParserDoesNotExist($className);
        }

        $this->logger->notice("Parsing model: {$className}");
        $modelParser = $this->modelParsers[$className];
        if ($modelParser instanceof AbstractPageParser) {
            $entities = $modelParser->parse($this->treeDir, $this->serializer);
        } else {
            $entities = $modelParser->parse($this->sourceDir, $this->serializer);
        }
        $this->dumpEntities($entities, $className);
    }

    private function dumpEntities(array $entities, $className)
    {
        $fs = new Filesystem();
        $fs->mkdir($this->dataDir);
        $filename = str_replace('DarkEye\\Schema\\', '', $className);
        $filename = str_replace('\\', '/', $filename);

        $content = json_encode($entities, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        $fs->dumpFile($this->dataDir.'/'.$filename.'.json', $content);
    }
}
