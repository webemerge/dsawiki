<?php

namespace DarkEye\Parser;

use HTMLPurifier;

interface PurifierAware
{
    public function setPurifier(HTMLPurifier $purifier);
}
