<?php

namespace DarkEye\Parser;

use Cocur\Slugify\SlugifyInterface;
use DarkEye\Entity\Entity;
use DarkEye\Exception\ParserException;
use DarkEye\Parser\Content\Page;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\Serializer;

abstract class AbstractTableParser implements ModelParser, SlugifyAware, LoggerAwareInterface
{
    use LoggerAwareTrait;

    const CONTENT_TABLE_SELECTOR = '#main > .mod_article > .block > table';

    /**
     * @var SlugifyInterface
     */
    private $slugify;

    /**
     * @return string
     */
    abstract protected function getPageFilename();

    public function setSlugify(SlugifyInterface $slugify)
    {
        $this->slugify = $slugify;
    }

    /**
     * @param string $sourceDir
     *
     * @return Entity[]
     */
    public function parse($sourceDir, Serializer $serializer)
    {
        $rawTableData = $this->loadTable($sourceDir);
        $sections = $this->detectSections($rawTableData);

        $entities = [];
        foreach ($sections as $section) {
            $entities = array_merge($entities, $this->parseSection($section));
        }

        return $entities;
    }

    /**
     * Load source file, find main table and convert table rows to array.
     *
     * @param $sourceDir
     *
     * @return array
     */
    protected function loadTable($sourceDir)
    {
        $file = realpath($sourceDir.'/'.$this->getPageFilename());
        $fs = new Filesystem();
        if (!$fs->exists($file)) {
            throw ParserException::fileNotFound($file);
        }

        $crawler = new Crawler(file_get_contents($file));

        return $crawler->filter(self::CONTENT_TABLE_SELECTOR.' tr')->each(function (Crawler $rowNode) {
            return $this->rowToArray($rowNode);
        });
    }

    protected function rowToArray(Crawler $rowNode)
    {
        return $rowNode->filter('td')->each(function (Crawler $child) {
            return trim($child->html());
        });
    }

    protected function detectSections(array $rawTableData)
    {
        $sections = [];
        $section = 0;
        $lastHeaders = [];
        foreach ($rawTableData as $i => $rawTableDatum) {
            $row = $rawTableDatum;
            $nextRow = $rawTableData[$i + 1] ?? null;
            if ($this->isSectionHeaderRow($row) && null !== $nextRow) {
                ++$section;

                if ($this->isHeaderRow($nextRow)) {
                    $headers = $this->mapHeaders($nextRow);
                    $lastHeaders = $headers;
                    $sections[$section] = [
                        'name' => $this->stripHtml($row[0]),
                        'headers' => $headers,
                        'rows' => [],
                        'comment' => '',
                    ];

                    // skip header row
                    ++$i;

                    continue;
                } else {
                    $sections[$section] = [
                        'name' => $this->stripHtml($row[0]),
                        'headers' => $lastHeaders,
                        'rows' => [],
                        'comment' => '',
                    ];

                    continue;
                }
            }
            if ($this->isCommentRow($row)) {
                $sections[$section]['comment'] = $row[0];

                continue;
            }
            if ($this->isEmptyRow($row)) {
                continue;
            }
            $sections[$section]['rows'][] = array_map([$this, 'stripHtml'], $row);
        }

        return array_values($sections);
    }

    protected function isSectionHeaderRow(array $tableRow)
    {
        $strippedRow = array_map([$this, 'stripHtml'], $tableRow);
        $nonEmptyCount = count(array_filter($strippedRow));

        return 1 === $nonEmptyCount && preg_match('@^<strong>.*</strong>@', $tableRow[0]);
    }

    protected function isHeaderRow(array $tableRow)
    {
        $strippedRow = array_map([$this, 'stripHtml'], $tableRow);
        $nonEmptyCount = count(array_filter($strippedRow));

        return 4 === $nonEmptyCount && preg_match('@^<strong>.*</strong>@', $tableRow[0]);
    }

    protected function isCommentRow($tableRow)
    {
        $strippedRow = array_map([$this, 'stripHtml'], $tableRow);
        $nonEmptyCount = count(array_filter($strippedRow));

        return 1 === $nonEmptyCount && preg_match('@^\*\)@', $tableRow[0]);
    }

    protected function isEmptyRow($tableRow)
    {
        $strippedRow = array_map([$this, 'stripHtml'], $tableRow);
        $nonEmptyCount = count(array_filter($strippedRow));

        return 0 === $nonEmptyCount;
    }

    protected function stripHtml($text)
    {
        $text = strip_tags($text);
        $text = str_replace("\xC2\xA0", ' ', $text);

        return trim($text);
    }

    protected function mapHeaders(array $headerRow)
    {
        $columns = new ColumnDefinition();
        $mappedHeaders = [];
        foreach ($headerRow as $header) {
            $header = $this->stripHtml($header);
            $name = $columns->mapColumnLabelToName($header);
            if (null === $name) {
                $this->logger->error('Unknown header: '.$header.' in table');
                dump($headerRow);
                die();
            }

            $mappedHeaders[] = $name;
        }

        return $mappedHeaders;
    }

    protected function parseSection(array $section)
    {
        $entities = [];
        foreach ($section['rows'] as $row) {
            while (count($row) < count($section['headers'])) {
                $row[] = '';
            }
            $values = array_combine($section['headers'], $row);
            $values = $this->handleSingleRowValues($values, $section);
            if (isset($values['publication'])) {
                $values['publication'] = str_replace(', Seite', ' Seite', $values['publication']);
            }
            $values['wiki_url'] = sprintf(Page::WIKI_URL_SCHEMA, $this->getPageFilename());

            $entities[] = $this->hydrateRow($values);
        }

        return $entities;
    }

    /**
     * Handle row values before hydration. Can be used to swap or insert fields.
     *
     * @param $row
     */
    protected function handleSingleRowValues($row, array $section)
    {
        return $row;
    }

    protected function hydrateRow(array $values)
    {
        $id = $this->slugify->slugify($values['name']);

        return new Entity($id, $values['name'], $values);
    }
}
