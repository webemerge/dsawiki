<?php

namespace DarkEye\Parser;

use DarkEye\Entity\Entity;
use DarkEye\Parser\Content\Page;
use DarkEye\Parser\Content\Paragraph;
use DarkEye\Parser\Content\Section;
use DarkEye\Parser\PageProvider\DirectoryPageProvider;
use HTMLPurifier;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\VarDumper\VarDumper;

/**
 * Parses the page for a single model item.
 */
abstract class AbstractPageParser implements ModelParser, PurifierAware, LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var HTMLPurifier
     */
    protected $purifier;

    /**
     * @return string[]
     */
    abstract protected function getSourceDirectories(): array;

    public function setPurifier(HTMLPurifier $purifier)
    {
        $this->purifier = $purifier;
    }

    /**
     * @param string $treeDir
     *
     * @return DirectoryPageProvider
     */
    public function getPageProvider($treeDir, Serializer $serializer)
    {
        return new DirectoryPageProvider($treeDir, $this->getSourceDirectories(), $serializer);
    }

    /**
     * @param string $treeDir
     *
     * @return Entity[]
     */
    public function parse($treeDir, Serializer $serializer)
    {
        $entities = [];
        foreach ($this->getPageProvider($treeDir, $serializer)->findPages() as $page) {
            $this->logger->info('Parsing page '.$page->getPath());
            $entity = $this->parseSingle($page);
            if (null !== $entity) {
                $entities[] = $entity;
            }
        }

        return $entities;
    }

    /**
     * @return Entity|null
     */
    protected function parseSingle(Page $page)
    {
        $sections = $page->getSections();
        if (empty($sections)) {
            return null;
        }
        $sections = $this->mergeSingleValueSectionsWithMain($sections);
        $sections = $this->fixSections($sections, $page);

        $values = $this->mapSectionsToValues($sections, $page);
        $values = $this->cleanValues($values);
        if (empty($values)) {
            return null;
        }

        return new Entity($page->getName(), $values['name'], $values);
    }

    /**
     * @param Section[] $sections
     *
     * @return Section[]
     */
    protected function fixSections(array $sections, Page $page)
    {
        return $sections;
    }

    protected function cleanValues($values)
    {
        if (isset($values['publication'])) {
            $values['publication'] = str_replace(', Seite', ' Seite', $values['publication']);
        }

        return array_map(function ($value) {
            if (is_string($value)) {
                $value = trim($value);
                $value = preg_replace('/(<br>)+$/', '', $value);
                if (false !== strpos($value, '<') || false !== strpos($value, '>')) {
                    $value = $this->purifier->purify($value);
                }

                return $value;
            }
            if (is_array($value)) {
                return $this->cleanValues($value);
            }

            return $value;
        }, $values);
    }

    /**
     * @param Section[] $sections
     *
     * @return array
     */
    protected function mapSectionsToValues(array $sections, Page $page)
    {
        $values = [];
        if (!isset($sections[0])) {
            return $values;
        }

        $this->fixMissingTitle($sections[0], $page);
        $this->mapSingleSection($sections[0], $values, $page);

        for ($i = 1, $iMax = count($sections); $i < $iMax; ++$i) {
            $this->mapFurtherSection($sections[$i], $values, $i, $page);
        }
        $values['wiki_url'] = $page->getWikiUrl();

        return $values;
    }

    protected function mapSingleSection(Section $section, array &$values, Page $page, bool $skipDuplicates = true)
    {
        $columns = new ColumnDefinition();

        $values['name'] = $section->getTitle();
        $lastName = null;
        foreach ($section->getParagraphs() as $paragraph) {
            $content = $paragraph->getContent();
            $label = null;
            $value = null;

            if (!$paragraph->mayHaveLabel() && null === $lastName) {
                if ($section->getTitle() === $content) {
                    // sometimes the first paragraph contains the headline again
                    continue;
                }
                $label = 'Regel';
                $value = $content;
            }

            if (null !== $lastName && !$this->isDefaultColumnAvailable() && $this->paragraphShouldBeMergedWithPrevious($paragraph)) {
                if (preg_match('@^\d+ QS:@', $content)) {
                    $content = preg_replace('@^(\d+) QS:(.*)$@', 'QS \1:\2', $content);
                }
                $values[$lastName] .= "<br>\n".$content;

                continue;
            }

            if (!isset($label)) {
                if (!$paragraph->mayHaveLabel()) {
                    if ($this->isDefaultColumnAvailable()) {
                        $this->addParagraphToDefaultColumn($values, $paragraph);

                        continue;
                    }

                    $this->exit(
                        'Found unusable paragraph: '.$content,
                        $paragraph->mayHaveLabel(),
                        $section
                    );
                }

                [$label, $value] = explode(':', $content, 2);
                $label = strip_tags($label);
            }

            $columnName = $columns->mapColumnLabelToName($label);
            if (null === $columnName) {
                if ($this->isDefaultColumnAvailable()) {
                    $this->addParagraphToDefaultColumn($values, $paragraph);

                    continue;
                }

                $this->exit(
                    'Unknown label: '.$label.' in paragraph '.$content.' on page '.$page->getName(),
                    $section
                );
            }

            $value = trim($value);
            if (isset($values[$columnName])) {
                if (!$skipDuplicates || $values[$columnName] !== $value) {
                    $values[$columnName] .= "<br>\n".$value;
                }
            } else {
                $values[$columnName] = $value;
            }
            $lastName = $columnName;
        }
    }

    protected function paragraphShouldBeMergedWithPrevious(Paragraph $paragraph): bool
    {
        if (!$paragraph->mayHaveLabel()) {
            return true;
        }

        $mergePatterns = [
            '@^QS [0-9]+:@',
            '@^QS [0-9]{1}-[0-9]{1}:@',
            '@^[0-9]{1}-[0-9]{1} QS:@',
            '@^[0-9]+ QS:@',
            '@^Stufe [0-9]{1}:@',
            '@^Stufe [IVX]+:@',
            '@^#[0-9]{1} Stufe@',
            '@^[0-9]{1}:@',
            '@^[0-9]{1}-[0-9]{1}:@',
            '@^#.* [0-9]+ QS:@',
            '@^\[siehe Anmerkungen:@',
            '@^<li>.*</li>$@s',
        ];

        $content = $paragraph->getContent();
        foreach ($mergePatterns as $pattern) {
            if (preg_match($pattern, $content)) {
                return true;
            }
        }

        return false;
    }

    protected function getDefaultColumnForUnknownParagraphs(): ?string
    {
        return null;
    }

    protected function isDefaultColumnAvailable(): bool
    {
        return null !== $this->getDefaultColumnForUnknownParagraphs();
    }

    protected function addParagraphToDefaultColumn(array &$values, Paragraph $paragraph): void
    {
        $content = strip_tags($paragraph->getContent());
        $defaultColumn = $this->getDefaultColumnForUnknownParagraphs();
        if (isset($values[$defaultColumn])) {
            $values[$defaultColumn] .= "<br>\n".$content;
        } else {
            $values[$defaultColumn] = $content;
        }
    }

    /**
     * Override depending on model.
     */
    protected function mapFurtherSection(Section $section, array &$values, int $index, Page $page)
    {
    }

    protected function detectParagraphs(Crawler $nodes)
    {
        $paragraphs = [];
        $nodes = $nodes->reduce(function (Crawler $subNodes) {
            $text = trim($subNodes->text());
            $text = trim($text, "\xC2\xA0");

            return '' != $text;
        });

        foreach ($nodes as $node) {
            $nodeCrawler = new Crawler($node);
            $html = $nodeCrawler->html();
            if (mb_substr_count($html, '<br><strong>') > 1) {
                // if there are at least 2 occurences of <br><strong> we assume these should be separate blocks
                $first = true;
                foreach (explode('<br><strong>', $html) as $part) {
                    if (!$first) {
                        $part = '<strong>'.$part;
                    }
                    $first = false;
                    $paragraphs[] = $part;
                }
            } else {
                $paragraphs[] = $html;
            }
        }

        return array_map(function ($paragraph) {
            return trim($this->stripFirstStrong($this->cleanHtml($paragraph)));
        }, $paragraphs);
    }

    protected function cleanHtml($html)
    {
        $html = preg_replace('@<span[^>]+>@s', '', $html);
        $html = preg_replace('@</span>@s', '', $html);
        $html = str_replace('</strong><strong>', '', $html);
        $html = preg_replace('@<!--.*-->@Us', '', $html);

        return trim($html, "\xC2\xA0");
    }

    protected function stripFirstStrong($text)
    {
        return preg_replace('@^<strong>(.*)\:[ ]{0,1}</strong>[ ]{0,1}(.*)@', '\1: \2', $text);
    }

    protected function contentToParagraphs(array $paragraphs)
    {
        return array_map(function ($paragraph) {
            if ($paragraph instanceof Paragraph) {
                return $paragraph;
            }

            return new Paragraph($paragraph);
        }, $paragraphs);
    }

    protected function fixMissingTitle(Section $section, Page $page)
    {
        if (0 === strpos($section->getTitle(), 'Autogenerated')) {
            if (!$section->getParagraphs()[0]->mayHaveLabel() && \strlen($section->getParagraphs()[0]) < 40) {
                $section->useFirstParagraphAsTitle();
            } else {
                $section->setTitle($page->getTitle());
            }
            $this->logger->info('Fixing title to '.$section->getTitle().' for page '.$page->getName());
        }
    }

    /**
     * @param Section[] $sections
     *
     * @return Section[]
     */
    protected function fixPublicationSection(array $sections): array
    {
        foreach ($sections as $index => $section) {
            if ($index > 0 && ('Publikation' === $section->getTitle() || 'Publikationen' === $section->getTitle())) {
                $this->logger->debug('Moving publication section to publication paragraph');
                $sections[0]->addParagraph(new Paragraph('Publikation: '.implode(', ', $section->getParagraphs())));
                unset($sections[$index]);
            }
        }

        return array_values($sections);
    }

    /**
     * @param Section[] $sections
     *
     * @return Section[]
     */
    protected function fixNotesSection(array $sections): array
    {
        foreach ($sections as $index => $section) {
            if ($index > 0 && ('Anmerkungen' === $section->getTitle() || 'Anmerkungen der Redaktion' === $section->getTitle())) {
                $sections[0]->addParagraph(new Paragraph('Anmerkung: '.$section->joinParagraphs()));
                unset($sections[$index]);
            }
        }

        return array_values($sections);
    }

    protected function dump(...$vars): void
    {
        foreach ($vars as $var) {
            VarDumper::dump($var);
        }
    }

    protected function exit(...$vars): void
    {
        foreach ($vars as $var) {
            VarDumper::dump($var);
        }

        die(1);
    }

    /**
     * @param Section[] $sections
     *
     * @return Section[]
     */
    protected function addParentTitleAsProperty($sections, Page $page, string $columnName): array
    {
        if (0 === count($sections)) {
            $sections[0] = new Section($page->getTitle());
        }

        $segments = $page->getBreadcrumb()->getSegments();
        if (count($segments) < 2) {
            return $sections;
        }
        array_pop($segments);
        $sections[0]->addParagraph(new Paragraph($columnName.': '.array_pop($segments)->getTitle()));

        return $sections;
    }

    /**
     * @param Section[] $sections
     *
     * @return Section[]
     */
    private function mergeSingleValueSectionsWithMain(array $sections): array
    {
        /* @var $newSections Section[] */
        $newSections = [];
        foreach ($sections as $index => $section) {
            if ($index > 0 && $section->mayBeSingleValueSection()) {
                $newSections[0]->addParagraph($section->convertToSingleValueParagraph());
                continue;
            }

            $newSections[] = $section;
        }

        return $newSections;
    }
}
