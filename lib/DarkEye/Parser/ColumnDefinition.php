<?php

namespace DarkEye\Parser;

use DarkEye\Schema\AbstractSchema;
use Symfony\Contracts\Translation\TranslatorInterface;

final class ColumnDefinition
{
    public const MAX_LABEL_LENGTH = 40;

    const PUBLICATION_SHORTCUTS = [
        'Aventurisches Götterwirken' => 'AG',
        'Aventurische Magie' => 'AM',
        'Aventurische Namen' => 'AN',
        'Aventurische Rüstkammer' => 'AR',
        'Aventurische Bestiarium' => 'AB',
        'Aventurische Tiergefährten' => 'AT',
        'Aventurisches Bestiarium' => 'AB',
        'Aventurisches Kompendium' => 'AK',
        'Aventurisches Pandämonium' => 'AP',
        'Aventurischer Almanach' => 'AA',
        'Aveturische Namen' => 'AN',
        'Bestiarium' => 'AB',
        'Bonusmaterial und Errata' => 'BoErr',
        'Das blaue Buch' => 'A[DbB]',
        'Das Erbe der Theaterritter' => 'DEdT',
        'Der Apfelwurm von Alriksfurt' => 'AHw[DAvA]',
        'Der dunkle Mhanadi' => 'A[DdM]',
        'Der grüne Zug' => 'A[DgZ]',
        'Der Grüne Zug' => 'A[DgZ]',
        'Der Rote Chor' => 'A[DrC]',
        'Der Rote Schlächter' => 'AS[DrS]',
        'Der schwarze Forst' => 'A[DsF]',
        'Der Weiße See' => 'A[DwS]',
        'Der weiße See' => 'A[DwS]',
        'Des Wandelbaren Schicksal' => 'AHw[DWS]',
        'Die Flusslande' => 'DFl',
        'Die Siebenwindküste' => 'DSk',
        'Die silberne Wehr' => 'A[DsW]',
        'Ein Fest für die Augen' => 'AHw[EFfdA]',
        'Ewiger Hass' => 'A[EH]',
        'Glaube, Macht &amp; Heldenmut - Die Streitenden Königreiche' => 'GMH:DSK',
        'Glaube; Macht und Heldentum: Die Streitenden Königreiche' => 'GMH:DSK',
        'Das Dornenreich' => 'DDr',
        'Die Streitenden Königreiche' => 'DSK',
        'Die streitenden Königreiche' => 'DSK',
        'Die Streitenen Königreiche - Nostria und Andergast' => 'DSK',
        'Donnerwacht 2 - Das Bündnis der Wacht' => 'A[Dw2]',
        'Drachenwerk &amp; Räuberpack' => 'A[D&amp;R]',
        'DSA5-Meisterschirm' => 'Meisterschirm',
        'Fauler Frühling' => 'AHw[FF]',
        'Gefangen in der Gruft der Königin' => 'A[GidGdK]',
        'Glaube, Macht &amp; Heldenmut - Die Siebenwindküste' => 'GMH:DSwk',
        'Glaube, Macht und Heldentum: Die Siebenwindküste' => 'GMH:DSwk',
        'Glaube, Macht und Heldenmut: Die Siebenwindküste' => 'GMH:DSwk',
        'Havena - Versunkene Geheimnisse' => 'HvG',
        'Havena Versunkene Geheimnisse' => 'HvG',
        'Kaiser der Diebe' => 'A[KdD]',
        'Katakomben &amp; Ruinen' => 'KuR',
        'Kibakadabra' => 'AHw[Kk]',
        'Klingen der Nacht' => 'A[KdN]',
        'Krallenspuren' => 'A[Ks]',
        'Nachtschwarze See' => 'A[NsS]',
        'Niobaras Vermächtnis' => 'A[NV]',
        'Offenbarung des Himmels' => 'A[OdH]',
        'Quelle des Nagrach' => 'A[QdN]',
        'Regelwerk' => 'GRW',
        'Seelanders Eleven' => 'AHw[SE]',
        'Seemannsgarn &amp; Krakensilber' => 'SuK',
        'Tyrannenmord' => 'AHw[Tm]',
        'Unendlichkeit und Tiefenrausch' => 'UuT',
        'Verräter und Geächtete' => 'A[VuG]',
        'Verräter und Geächtet' => 'A[VuG]',
        'Wege der Vereinigungen' => 'WdV',
        //'Wege der Vereinigung' => 'WdV',
        '(Neuauflage)' => 'Neu',
        '(2. überarb. Auflage)' => '2üA',
        '(3. überarb. Aufl.)' => '3üA',
        '(3. überar. Auflage)' => '3üA',
        '(3. überarbeitet Auflage)' => '3üA',
        '(3. überarbeitete Auflage)' => '3üA',
    ];

    private $columns = [
        'actions' => 'Aktionen',
        'additional_drain' => 'zusätzliche Abzüge',
        'advantage_type' => 'advantage_type',
        'alphabet' => 'Alphabet',
        'ammunition' => 'Munition',
        'animal_types' => 'Tierarten',
        'ap_cost' => 'AP-Wert',
        'assigned_language' => 'zugeordnete Sprache',
        'asp_cost' => 'AsP-Kosten',
        'aspect' => 'Aspekt',
        'attack_parry_modifiers' => 'AT/PA-Mod',
        'armor_advantage' => 'Rüstungsvorteil',
        'armor_disadvantage' => 'Rüstungsnachteil',
        'armor_level' => 'Rüstungsschutz',
        'armor_type' => 'armor_type',
        'armor_type_' => 'Art',
        'banning_circle' => 'Bannkreis',
        'binding_volume' => 'Volumen',
        'binding_ap_cost' => 'Bindungskosten',
        'brew' => 'Gebräu',
        'burden_level' => 'Belastung (Stufe)',
        'burden_level_' => 'Belastungsstufe',
        'ceremony_duration' => 'Zeremoniedauer',
        'ceremony_duration_' => 'Zeremoniendauer',
        'ceremony_extensions' => 'Zeremonieerweiterungen',
        'circle_of_damnation' => 'Kreis',
        'circulation' => 'Verbreitung',
        'codes_of_ethics' => 'Moralkodici',
        'complexity' => 'Komplexität',
        'cost_modifiable' => 'K. modifizierbar',
        'creature_sub_type' => 'creature_sub_type',
        'creature_type' => 'creature_type',
        'creature_type_long' => 'Typus',
        'creature_type_long_' => 'Kreaturenarten',
        'damage' => 'TP',
        'description' => 'Beschreibung',
        'difficulty' => 'Erschwernis',
        'disadvantage_examples' => 'Beispiele',
        'disadvantage_type' => 'disadvantage_type',
        'dm_info' => 'Meistermaske',
        'duration' => 'Dauer',
        'effect' => 'Wirkung',
        'effective_duration' => 'Wirkungsdauer',
        'errata' => 'Errata',
        'errata_' => '* Errata',
        'errata__' => '["Vorab-Errata" durch Redaktion',
        'errata___' => 'Änderung',
        'errata_box' => 'Errata Meisterkasten',
        'extended_fighting_skills' => 'Erweiterte Kampfsonderfertigkeiten',
        'extended_magic_skills' => 'Erweiterte Zaubersonderfertigkeiten',
        'extended_magic_style_skills' => 'Erweiterte Zauberstilsonderfertigkeiten',
        'extended_liturgy_skills' => 'Erweiterte Liturgiesonderfertigkeiten',
        'extended_talent_skills' => 'Erweiterte Talentsonderfertigkeiten',
        'fighting_styles' => 'Kampftechniken',
        'fighting_styles_' => 'Kampftechnik',
        'identification_modifier' => 'Bestimmungsschwierigkeit',
        'is_improvised' => 'is_improvised',
        'is_passive' => 'is_passive',
        'kap_cost' => 'KaP-Kosten',
        'language' => 'Sprache',
        'language_variations' => 'Sprachspezialisierung',
        'length' => 'Länge',
        'liturgy_duration' => 'Liturgiedauer',
        'liturgy_duration_' => 'Liturgiedauerdauer',
        'liturgy_extensions' => 'Liturgieerweiterungen',
        'loading_time' => 'LZ',
        'main_attribute_damage_treshold' => 'L+S',
        'music_tradition' => 'Musiktradition',
        'note' => 'Anmerkung',
        'number_of_uses' => 'Anwendungen',
        'price' => 'Preis',
        'protection_circle' => 'Schutzkreis',
        'publication' => 'Publikation',
        'publication_' => 'Publikationen',
        'raise_factor' => 'Steigerungsfaktor',
        'reach' => 'Reichweite',
        'reach_' => 'RW',
        'requirements' => 'Voraussetzungen',
        'requirements_' => 'Voraussetzung',
        'ritual_duration' => 'Ritualdauer',
        'rule' => 'Regel',
        'rule_' => '*) Ausnahme',
        'rule__' => 'Regeltechnik',
        'script' => 'Schrift',
        'search_modifier' => 'Suchschwierigkeit',
        'size' => 'Größe',
        'size_category' => 'Größenkategorie',
        'skill_type' => 'skill_type',
        'spell_type' => 'spell_type',
        'sub_type' => 'sub_type',
        'spell_duration' => 'Zauberdauer',
        'spell_extensions' => 'Zaubererweiterungen',
        'talent' => 'Talent',
        'target_category' => 'Zielkategorie',
        'test_for' => 'Probe',
        'trade_secret' => 'Berufsgeheimnis',
        'trait' => 'Merkmal',
        'tribal_tradition' => 'Stammestradition',
        'upgrade_cost' => 'Steigerungskosten',
        'usable_by' => 'verwendbar durch',
        'weapon_advantage' => 'Waffenvorteil',
        'weapon_disadvantage' => 'Waffennachteil',
        'weapon_type' => 'weapon_type',
        'weight' => 'Gewicht',
        'wiki_url' => 'Wiki-URL',
    ];

    const ATTRIBUTES = [
        'MU',
        'KL',
        'IN',
        'CH',
        'FF',
        'GE',
        'KO',
        'KK',
    ];

    /**
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @param string $label
     *
     * @return string|null
     */
    public function mapColumnLabelToName($label)
    {
        $name = array_search($label, $this->columns);
        if (false === $name) {
            return null;
        }

        return rtrim($name, '_');
    }

    /**
     * Generate DataTable column definitions for the given schema.
     *
     *
     * @return array
     */
    public function getColumnSettingsForSchema(AbstractSchema $schema, TranslatorInterface $translator)
    {
        $columns = [];

        $columns[] = [
            'className' => 'details-control',
            'orderable' => false,
            'data' => null,
            'defaultContent' => '<i class="fa fa-plus-circle"></i>',
        ];

        $visibleFields = $schema->getDefaultTableFieldNames();
        $fieldNames = array_merge($schema->getFieldNames(), $this->getDefaultFieldNames());
        foreach ($fieldNames as $fieldName) {
            $columns[] = [
                'data' => 'data.'.$fieldName,
                'title' => $this->getColumnTitle($fieldName, $translator),
                'defaultContent' => '',
                'visible' => in_array($fieldName, $visibleFields),
                'className' => 'table-field-'.$fieldName,
            ];
        }

        return $columns;
    }

    /**
     * Default field names for all schemas.
     *
     * @return string[]
     */
    private function getDefaultFieldNames(): array
    {
        return [
            'wiki_url',
        ];
    }

    private function getColumnTitle($fieldName, TranslatorInterface $translator)
    {
        $transName = 'column.'.$fieldName;
        $title = $translator->trans($transName);
        if ($title === $transName) {
            $title = $this->columns[$fieldName] ?? $fieldName;
        }

        return $title;
    }

    /**
     * @return array
     */
    public function getPublicationShortcuts()
    {
        return self::PUBLICATION_SHORTCUTS;
    }

    /**
     * @return array
     */
    public function getAttributes(TranslatorInterface $translator)
    {
        $attributes = [];
        foreach (self::ATTRIBUTES as $attr) {
            $attributes[$attr] = $translator->trans('attribute.'.$attr);
        }

        return $attributes;
    }
}
