<?php

namespace DarkEye\Parser\Content;

final class PathSegment
{
    private $slug;

    private $title;

    private $link;

    public function __construct(string $slug, string $title, string $link)
    {
        $this->slug = $slug;
        $this->title = $title;
        $this->link = $link;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function __toString(): string
    {
        return $this->title;
    }
}
