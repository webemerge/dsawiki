<?php

namespace DarkEye\Parser\Content;

final class Section
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var Paragraph[]
     */
    private $paragraphs = [];

    /**
     * Section constructor.
     *
     * @param Paragraph[] $paragraphs
     */
    public function __construct(string $title, array $paragraphs = [])
    {
        $this->title = $title;
        $this->setParagraphs($paragraphs);
    }

    public function addParagraph(Paragraph $paragraph): self
    {
        $this->paragraphs[] = $paragraph;

        return $this;
    }

    public function removeParagraph(Paragraph $paragraphToRemove): self
    {
        $allParagraphs = $this->paragraphs;
        $this->paragraphs = [];
        foreach ($allParagraphs as $paragraph) {
            if ($paragraph !== $paragraphToRemove) {
                $this->addParagraph($paragraph);
            }
        }

        return $this;
    }

    /**
     * @param Paragraph[] $paragraphs
     */
    public function setParagraphs(array $paragraphs): self
    {
        $this->paragraphs = [];
        foreach ($paragraphs as $paragraph) {
            $this->addParagraph($paragraph);
        }

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return Paragraph[]
     */
    public function getParagraphs(): array
    {
        return $this->paragraphs;
    }

    public function toHtml(): string
    {
        return '<h3>'.$this->getTitle()."</h3>\n".$this->joinParagraphs();
    }

    public function joinParagraphs(string $separator = "<br>\n"): string
    {
        $html = '';
        foreach ($this->getParagraphs() as $paragraph) {
            $html .= $paragraph.$separator;
        }
        if (mb_strlen($html) > 0) {
            $html = mb_substr($html, 0, strlen($separator) * -1);
        }

        return trim($html);
    }

    public function appendToParagraphThatStartsWith($startsWith, $appendHtml): self
    {
        foreach ($this->paragraphs as $i => $paragraph) {
            if ($paragraph->startsWith($startsWith)) {
                $paragraph->append("<br>\n".$appendHtml);

                return $this;
            }
        }

        throw new \RuntimeException('Could not find paragraph starting with "'.$startsWith.'"');
    }

    /**
     * @return Paragraph[]
     */
    public function extractParagraphs(string $fromPattern, string $untilPattern = '#^this will not match$#'): array
    {
        $extracted = [];
        $keep = [];
        $started = false;
        foreach ($this->paragraphs as $paragraph) {
            if ($started && preg_match($untilPattern, $paragraph)) {
                $started = false;
            }

            if ($started || preg_match($fromPattern, $paragraph)) {
                $started = true;
                $extracted[] = $paragraph;

                continue;
            }

            $keep[] = $paragraph;
        }

        $this->setParagraphs($keep);

        return $extracted;
    }

    /**
     * Extract the first paragraph that matches the given pattern.
     */
    public function extractFirstParagraph(string $fromPattern): ?Paragraph
    {
        $extracted = null;
        $keep = [];
        foreach ($this->paragraphs as $paragraph) {
            if (null === $extracted && preg_match($fromPattern, $paragraph)) {
                $extracted = $paragraph;

                continue;
            }

            $keep[] = $paragraph;
        }

        $this->setParagraphs($keep);

        return $extracted;
    }

    /**
     * Use first paragraph content as section title.
     */
    public function useFirstParagraphAsTitle(): self
    {
        $this->title = $this->paragraphs[0]->getContent();
        unset($this->paragraphs[0]);
        $this->paragraphs = array_values($this->paragraphs);

        return $this;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function hasContent(): bool
    {
        return count($this->paragraphs) > 0;
    }

    public function isAutoGenerated(): bool
    {
        return 0 === strpos($this->title, 'Autogenerated');
    }

    public function isList(): bool
    {
        if (!$this->hasContent()) {
            return false;
        }

        return $this->paragraphs[0]->startsWithAny(['<ul>', '<ol>']);
    }

    public function convertListToParagraphs(): self
    {
        if (!$this->isList()) {
            return $this;
        }

        $newParagraphs = [];
        foreach ($this->getParagraphs() as $paragraph) {
            if (!$paragraph->startsWithAny(['<ul>', '<ol>'])) {
                $newParagraphs[] = $paragraph;
                continue;
            }

            $content = strtr($paragraph->getContent(), [
                '<ul>' => '',
                '</ul>' => '',
                '<ol>' => '',
                '</ol>' => '',
                '</li>' => '',
            ]);

            $newLines = explode('<li>', $content);
            foreach ($newLines as $newLine) {
                $newLine = trim($newLine);
                if (!empty($newLine)) {
                    $newParagraphs[] = new Paragraph('#'.$newLine);
                }
            }
        }
        $this->setParagraphs($newParagraphs);

        return $this;
    }

    /**
     * Single value sections MUST contain 2 paragraphs: one holding the label and one holding the value. They MUST be
     * auto-generated. The first paragraph MUST look like a label.
     */
    public function mayBeSingleValueSection(): bool
    {
        if (!$this->isAutoGenerated() || 2 !== count($this->paragraphs)) {
            return false;
        }

        return $this->paragraphs[0]->mayBeLabel();
    }

    public function convertToSingleValueParagraph(): Paragraph
    {
        if (!$this->mayBeSingleValueSection()) {
            throw new \RuntimeException('This section does not qualify for a single value section.');
        }

        return new Paragraph($this->joinParagraphs(' '));
    }
}
