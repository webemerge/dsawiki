<?php

namespace DarkEye\Parser\Content;

final class Page
{
    const WIKI_URL_SCHEMA = 'https://ulisses-regelwiki.de/index.php/%s';

    /**
     * @var Breadcrumb
     */
    private $breadcrumb;

    /**
     * @var string
     */
    private $locale;

    /**
     * @var string
     */
    private $cleanedContent;

    /**
     * @var string
     */
    private $filename;

    /**
     * @var Section[]
     */
    private $sections = [];

    /**
     * Page constructor.
     *
     * @param Section[] $sections
     */
    public function __construct(
        Breadcrumb $breadcrumb,
        string $locale,
        string $cleanedContent,
        string $filename,
        array $sections = []
    ) {
        $this->breadcrumb = $breadcrumb;
        $this->locale = $locale;
        $this->cleanedContent = $cleanedContent;
        $this->filename = $filename;

        foreach ($sections as $section) {
            $this->addSection($section);
        }
    }

    public function getBreadcrumb(): Breadcrumb
    {
        return $this->breadcrumb;
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function getCleanedContent(): string
    {
        return $this->cleanedContent;
    }

    public function getFilename(): string
    {
        return $this->filename;
    }

    public function getWikiUrl(): string
    {
        return sprintf(self::WIKI_URL_SCHEMA, $this->getFilename());
    }

    public function getPath(): string
    {
        return sprintf('%s/%s', $this->locale, $this->breadcrumb->getSlug());
    }

    public function getTitle(): string
    {
        $segments = $this->breadcrumb->getSegments();

        return array_pop($segments)->getTitle();
    }

    public function getName(): string
    {
        $segments = $this->breadcrumb->getSegments();

        return array_pop($segments)->getSlug();
    }

    public function getParentName(): ?string
    {
        $segments = $this->breadcrumb->getSegments();
        array_pop($segments);
        if (count($segments) > 0) {
            return array_pop($segments)->getSlug();
        }

        return null;
    }

    public function getParentTitle(): ?string
    {
        $segments = $this->breadcrumb->getSegments();
        array_pop($segments);
        if (count($segments) > 0) {
            return array_pop($segments)->getTitle();
        }

        return null;
    }

    public function getMetadata(): array
    {
        return [
            'locale' => $this->locale,
            'breadcrumb' => $this->breadcrumb,
            'filename' => $this->filename,
        ];
    }

    /**
     * @return Section[]
     */
    public function getSections(): array
    {
        return $this->sections;
    }

    public function addSection(Section $section): void
    {
        $this->sections[] = $section;
    }
}
