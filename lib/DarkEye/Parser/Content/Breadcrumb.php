<?php

namespace DarkEye\Parser\Content;

final class Breadcrumb
{
    /**
     * @var PathSegment[]
     */
    private $segments = [];

    /**
     * @param PathSegment[] $segments
     */
    public function __construct($segments = [])
    {
        foreach ($segments as $segment) {
            $this->add($segment);
        }
    }

    public function add(PathSegment $segment)
    {
        $this->segments[] = $segment;
    }

    /**
     * @return PathSegment[]
     */
    public function getSegments()
    {
        return $this->segments;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        $slugs = array_map(function (PathSegment $segment) {
            return $segment->getSlug();
        }, $this->segments);

        return implode('/', $slugs);
    }
}
