<?php

namespace DarkEye\Parser\Content;

use DarkEye\Parser\ColumnDefinition;
use function Symfony\Component\String\u;
use Symfony\Component\String\UnicodeString;

final class Paragraph
{
    /**
     * @var UnicodeString
     */
    private $content;

    public function __construct(string $content)
    {
        $this->content = u($content)->trim();
    }

    public function getContent(): string
    {
        return $this->content->toString();
    }

    public function __toString()
    {
        return $this->getContent();
    }

    /**
     * Modify paragraph content.
     */
    public function replace(string $pattern, string $replacement): void
    {
        $this->content = $this->content->replaceMatches($pattern, $replacement);
    }

    /**
     * Append text to paragraph content.
     */
    public function append(string $text): void
    {
        $this->content = $this->content->append($text);
    }

    /**
     * Check if this paragraph may contain a label.
     */
    public function mayHaveLabel(): bool
    {
        return
            $this->containsBeforePos(':', ColumnDefinition::MAX_LABEL_LENGTH) &&
            !$this->content->startsWith('#') &&
            !$this->content->startsWith('<em>')
        ;
    }

    /**
     * Check if this paragraph may contain a label.
     */
    public function mayBeLabel(): bool
    {
        return
            $this->containsBeforePos(':', ColumnDefinition::MAX_LABEL_LENGTH) &&
            $this->content->endsWith(':') &&
            !$this->content->startsWith('#') &&
            !$this->content->startsWith('<em>')
        ;
    }

    public function startsWith(string $string): bool
    {
        return $this->content->startsWith($string);
    }

    public function startsWithAny(array $strings): bool
    {
        foreach ($strings as $string) {
            if ($this->content->startsWith($string)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if the given string occurs before $maxCharPos.
     */
    private function containsBeforePos(string $string, int $maxCharPos): bool
    {
        $pos = $this->content->indexOf($string);

        return null !== $pos && $pos < $maxCharPos;
    }
}
