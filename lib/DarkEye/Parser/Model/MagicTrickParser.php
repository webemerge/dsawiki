<?php

namespace DarkEye\Parser\Model;

use DarkEye\Parser\AbstractPageParser;
use DarkEye\Schema\MagicTrick;

final class MagicTrickParser extends AbstractPageParser
{
    use PagesContainingPrimaryEffectTrait;

    /**
     * @return string
     */
    public function getModelClass()
    {
        return MagicTrick::class;
    }

    /**
     * @return string[]
     */
    protected function getSourceDirectories(): array
    {
        return [
            'de/magie/zaubertricks',
        ];
    }
}
