<?php

namespace DarkEye\Parser\Model;

use DarkEye\Parser\AbstractPageParser;
use DarkEye\Parser\Content\Page;
use DarkEye\Parser\Content\Paragraph;
use DarkEye\Parser\Content\Section;
use DarkEye\Parser\PageProvider\NestedDirectoryParser;
use DarkEye\Schema\Creature;
use Symfony\Component\Serializer\Serializer;

final class CreatureParser extends AbstractPageParser
{
    use PagesContainingGenericDescriptionsAndSectionsTrait;

    /**
     * @return string
     */
    public function getModelClass()
    {
        return Creature::class;
    }

    /**
     * @param string $treeDir
     *
     * @return NestedDirectoryParser
     */
    public function getPageProvider($treeDir, Serializer $serializer)
    {
        return new NestedDirectoryParser($treeDir, $this->getSourceDirectories(), $serializer);
    }

    /**
     * @return string[]
     */
    protected function getSourceDirectories(): array
    {
        return [
            'de/bestiarium',
        ];
    }

    /**
     * @param \DarkEye\Parser\Content\Section[] $sections
     *
     * @return \DarkEye\Parser\Content\Section[]
     */
    protected function fixSections(array $sections, Page $page): array
    {
        $sections = $this->fixPublicationSection($sections);
        $sections = $this->mergeAdditionalSectionsIntoFirstSection($sections, $page);

        return $this->addCreatureType($sections, $page);
    }

    /**
     * @param Section[] $sections
     *
     * @return \DarkEye\Parser\Content\Section[]
     */
    private function addCreatureType($sections, Page $page): array
    {
        if (0 === count($sections)) {
            $sections[0] = new Section($page->getTitle());
        }

        $segments = $page->getBreadcrumb()->getSegments();
        array_pop($segments);
        if (count($segments) > 2) {
            $sections[0]->addParagraph(new Paragraph('creature_sub_type: '.array_pop($segments)->getTitle()));
        }
        if (count($segments) > 1) {
            $sections[0]->addParagraph(new Paragraph('creature_type: '.array_pop($segments)->getTitle()));
        }

        return $sections;
    }
}
