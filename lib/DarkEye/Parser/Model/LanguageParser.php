<?php

namespace DarkEye\Parser\Model;

use DarkEye\Parser\AbstractTableParser;
use DarkEye\Schema\Language;

final class LanguageParser extends AbstractTableParser
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Language::class;
    }

    /**
     * @return string
     */
    protected function getPageFilename()
    {
        return 'Sprachen.html';
    }

    protected function handleSingleRowValues($row, array $section)
    {
        if (isset($row['language'])) {
            $row['name'] = $row['language'];
            unset($row['language']);
        } elseif (isset($row['script'])) {
            $row['name'] = $row['script'];
            unset($row['script']);
        }

        $row['group'] = $section['name'];

        return $row;
    }
}
