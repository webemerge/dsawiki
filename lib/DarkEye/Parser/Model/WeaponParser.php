<?php

namespace DarkEye\Parser\Model;

use DarkEye\Parser\AbstractPageParser;
use DarkEye\Parser\Content\Page;
use DarkEye\Parser\Content\Paragraph;
use DarkEye\Parser\Content\Section;
use DarkEye\Parser\PageProvider\NestedDirectoryParser;
use DarkEye\Schema\Weapon;
use Symfony\Component\Serializer\Serializer;

final class WeaponParser extends AbstractPageParser
{
    public function getModelClass()
    {
        return Weapon::class;
    }

    /**
     * @param string $treeDir
     *
     * @return NestedDirectoryParser
     */
    public function getPageProvider($treeDir, Serializer $serializer)
    {
        return new NestedDirectoryParser($treeDir, $this->getSourceDirectories(), $serializer);
    }

    protected function getSourceDirectories(): array
    {
        return [
            'de/ruestkammer/waffen',
        ];
    }

    /**
     * @param Section[] $sections
     *
     * @return Section[]
     */
    protected function fixSections(array $sections, Page $page): array
    {
        $sections = $this->removeWeaponNames($sections, $page);
        $sections = $this->addWeaponType($sections, $page);

        return $this->fixBesen($sections, $page);
    }

    /**
     * @param Section[] $sections
     *
     * @return Section[]
     */
    private function removeWeaponNames(array $sections, Page $page): array
    {
        $section = $sections[0];

        $section->extractFirstParagraph('#^Waffe\:#');
        $section->extractFirstParagraph('#^Name\:#');

        return $sections;
    }

    /**
     * @param Section[] $sections
     *
     * @return Section[]
     */
    private function addWeaponType($sections, Page $page): array
    {
        $breadcrumb = $page->getBreadcrumb();
        $type = $breadcrumb->getSegments()[2]->getTitle();

        if (count($sections) >= 0) {
            $sections[0]->addParagraph(new Paragraph('weapon_type: '.$type));
        }

        return $sections;
    }

    /**
     * @param Section[] $sections
     *
     * @return Section[]
     */
    private function fixBesen(array $sections, Page $page): array
    {
        if ('besen-i' !== $page->getName()) {
            return $sections;
        }

        if (!isset($sections[1])) {
            return $sections;
        }

        $sections[1]->extractFirstParagraph("#^'#");

        return $sections;
    }

    protected function mapFurtherSection(Section $section, array &$values, int $index, Page $page)
    {
        $section->setTitle($page->getTitle());
        $this->mapSingleSection($section, $values, $page);

        if (!isset($values['is_improvised'])) {
            $values['is_improvised'] = strpos($page->getTitle(), '(i)') > 0;
        }
    }
}
