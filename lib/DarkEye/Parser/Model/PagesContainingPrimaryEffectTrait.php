<?php

namespace DarkEye\Parser\Model;

use DarkEye\Parser\Content\Page;
use DarkEye\Parser\Content\Section;

trait PagesContainingPrimaryEffectTrait
{
    /**
     * @param Section[] $sections
     *
     * @return Section[]
     */
    protected function fixSections(array $sections, Page $page)
    {
        return $this->prefixFirstParagraph($sections, $page);
    }

    /**
     * @param Section[] $sections
     *
     * @return Section[]
     */
    private function prefixFirstParagraph(array $sections, Page $page): array
    {
        $sections[0]->getParagraphs()[0]->replace('#^(.*)#', 'Wirkung: \1');

        return $sections;
    }
}
