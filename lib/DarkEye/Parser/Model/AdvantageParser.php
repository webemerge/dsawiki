<?php

namespace DarkEye\Parser\Model;

use DarkEye\Parser\AbstractPageParser;
use DarkEye\Parser\Content\Page;
use DarkEye\Parser\Content\Section;
use DarkEye\Schema\Advantage;

final class AdvantageParser extends AbstractPageParser
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Advantage::class;
    }

    /**
     * @return string[]
     */
    protected function getSourceDirectories(): array
    {
        return [
            'de/vor-und-nachteile/vorteile',
            'de/wege-der-vereinigungen-ab-18-jahre/wege-der-vereinigungen-regelteile/sex-vorteile',
        ];
    }

    /**
     * @param Section[] $sections
     *
     * @return \DarkEye\Parser\Content\Section[]
     */
    protected function fixSections(array $sections, Page $page)
    {
        $sections = $this->fixRequirements($sections);

        return $this->addParentTitleAsProperty($sections, $page, 'advantage_type');
    }

    /**
     * @param Section[] $sections
     *
     * @return Section[]
     */
    private function fixRequirements($sections)
    {
        foreach ($sections as $section) {
            foreach ($section->getParagraphs() as $paragraph) {
                $paragraph->replace('@^Voraussetzung\:@', 'Voraussetzungen:');
            }
        }

        return $sections;
    }
}
