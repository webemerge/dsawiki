<?php

namespace DarkEye\Parser\Model;

use DarkEye\Parser\AbstractPageParser;
use DarkEye\Parser\Content\Page;
use DarkEye\Parser\Content\Paragraph;
use DarkEye\Parser\Content\Section;
use DarkEye\Parser\PageProvider\NestedDirectoryParser;
use DarkEye\Schema\Armor;
use Symfony\Component\Serializer\Serializer;

final class ArmorParser extends AbstractPageParser
{
    public function getModelClass()
    {
        return Armor::class;
    }

    /**
     * @param string $treeDir
     *
     * @return NestedDirectoryParser
     */
    public function getPageProvider($treeDir, Serializer $serializer)
    {
        return new NestedDirectoryParser($treeDir, $this->getSourceDirectories(), $serializer, true);
    }

    protected function getSourceDirectories(): array
    {
        return [
            'de/ruestkammer/ruestung',
        ];
    }

    /**
     * @param Section[] $sections
     *
     * @return Section[]
     */
    protected function fixSections(array $sections, Page $page): array
    {
        return $this->addArmorType($sections, $page);
    }

    /**
     * @param Section[] $sections
     *
     * @return Section[]
     */
    private function addArmorType($sections, Page $page): array
    {
        $breadcrumb = $page->getBreadcrumb();
        $type = $breadcrumb->getSegments()[2]->getTitle();

        if (count($sections) >= 0) {
            $sections[0]->addParagraph(new Paragraph('armor_type: '.$type));
        }

        return $sections;
    }

    protected function mapFurtherSection(Section $section, array &$values, int $index, Page $page)
    {
        $section->setTitle($page->getTitle());
        $this->mapSingleSection($section, $values, $page);
    }
}
