<?php

namespace DarkEye\Parser\Model;

use DarkEye\Parser\AbstractPageParser;
use DarkEye\Parser\Content\Page;
use DarkEye\Parser\Content\Section;
use DarkEye\Schema\Plant;

final class PlantParser extends AbstractPageParser
{
    use PagesContainingGenericDescriptionsAndSectionsTrait;

    /**
     * @return string
     */
    public function getModelClass()
    {
        return Plant::class;
    }

    /**
     * @return string[]
     */
    protected function getSourceDirectories(): array
    {
        return [
            'de/herbarium/pflanzen',
        ];
    }

    /**
     * @param Section[] $sections
     *
     * @return Section[]
     */
    protected function fixSections(array $sections, Page $page): array
    {
        $sections = $this->fixPublicationSection($sections);

        return $this->mergeAdditionalSectionsIntoFirstSection($sections, $page);
    }
}
