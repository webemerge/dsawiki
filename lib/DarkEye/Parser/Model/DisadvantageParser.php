<?php

namespace DarkEye\Parser\Model;

use DarkEye\Parser\AbstractPageParser;
use DarkEye\Parser\Content\Page;
use DarkEye\Parser\Content\Section;
use DarkEye\Schema\Disadvantage;

final class DisadvantageParser extends AbstractPageParser
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Disadvantage::class;
    }

    /**
     * @return string[]
     */
    protected function getSourceDirectories(): array
    {
        return [
            'de/vor-und-nachteile/nachteile',
            'de/wege-der-vereinigungen-ab-18-jahre/wege-der-vereinigungen-regelteile/sex-nachteile',
        ];
    }

    /**
     * @param \DarkEye\Parser\Content\Section[] $sections
     *
     * @return \DarkEye\Parser\Content\Section[]
     */
    protected function fixSections(array $sections, Page $page)
    {
        $sections = $this->fixPublicationSection($sections);
        $sections = $this->fixRequirements($sections);
        $sections = $this->fixExamples($sections);
        $sections = $this->fixMutilated($sections, $page);

        return $this->addParentTitleAsProperty($sections, $page, 'disadvantage_type');
    }

    /**
     * @param \DarkEye\Parser\Content\Section[] $sections
     *
     * @return \DarkEye\Parser\Content\Section[]
     */
    private function fixRequirements($sections)
    {
        foreach ($sections as $section) {
            foreach ($section->getParagraphs() as $paragraph) {
                $paragraph->replace('@^Voraussetzung\:@', 'Voraussetzungen:');
                $paragraph->replace('@^Publikationen\:@', 'Publikation:');
            }
        }

        return $sections;
    }

    /**
     * @param \DarkEye\Parser\Content\Section[] $sections
     *
     * @return \DarkEye\Parser\Content\Section[]
     */
    private function fixExamples($sections)
    {
        if (!isset($sections[0]) || count($sections) > 1) {
            return $sections;
        }

        // this is for disadvantages including examples. they are moved into an extra section to ease parsing.
        $examples = $sections[0]->extractParagraphs('@^Beispiele@');
        if (count($examples) > 0) {
            $sections[] = new Section('Beispiele', array_slice($examples, 1));
        }

        return $sections;
    }

    /**
     * @param \DarkEye\Parser\Content\Section[] $sections
     *
     * @return array
     */
    private function fixMutilated(array $sections, Page $page)
    {
        if ('verstuemmelt' !== $page->getName()) {
            return $sections;
        }

        $examples = $sections[0]->extractParagraphs('@^#@', '@^[^#]+@');
        $examples = implode("<br>\n", $examples);

        $sections[0]->getParagraphs()[0]->append("<br>\n".$examples);

        return $sections;
    }

    /**
     * Override depending on model.
     */
    protected function mapFurtherSection(Section $section, array &$values, int $index, Page $page)
    {
        if (1 !== $index) {
            $this->logger->error('Found more than 2 sections:');
            $this->dump($section, $values);
        }

        $title = $section->getTitle();
        if (!in_array($title, ['Beispiele', 'Moralkodici', 'Publikation'])) {
            $this->logger->error('Unknown section name: '.$title);
            $this->dump($section, $values);
            die();
        }

        if ('Beispiele' === $title) {
            foreach ($section->getParagraphs() as $paragraph) {
                if (preg_match('@#([^\:]*)\:(.*)@', $paragraph, $matches)) {
                    $values['disadvantage_examples'][] = [
                        'name' => $matches[1],
                        'description' => $matches[2],
                    ];
                } elseif (preg_match('@^Publikation\: (.*)@', $paragraph, $matches)) {
                    $itemsCount = count($values['disadvantage_examples']);
                    for ($i = 0; $i < $itemsCount; ++$i) {
                        if (!isset($values['disadvantage_examples'][$i]['publication'])) {
                            $values['disadvantage_examples'][$i]['publication'] = $matches[1];
                        }
                    }
                } else {
                    $lastExample = count($values['disadvantage_examples']) - 1;
                    $values['disadvantage_examples'][$lastExample]['description'] .= "<br>\n".$paragraph;
                    // dump('Unknown disadvantage example paragraph: '.$paragraph);
                    // dump($section);
                    // dump($values);
                    // die();
                }
            }
        }
        if ('Moralkodici' === $title) {
            $values['codes_of_ethics'] = [];
            $god = null;
            foreach ($section->getParagraphs() as $paragraph) {
                if (preg_match('@^(Moralkodex der (.*)geweihten)$@', $paragraph, $matches)) {
                    $god = (string) $matches[2];
                    $values['codes_of_ethics'][$god]['heading'] = $matches[1];
                } elseif (preg_match('@^([^\:]*): (.*)@', $paragraph, $matches)) {
                    if (null === $god) {
                        $this->logger->error('Code of ethics without prior assigned god: '.$paragraph);
                        $this->dump($section, $values);
                        die();
                    }
                    $values['codes_of_ethics'][$god][$matches[1]] = $matches[2];
                } else {
                    $this->logger->error('Unknown codes of ethics paragraph: '.$paragraph);
                    $this->dump($section, $values);
                    die();
                }
            }
        }
        if ('Publikation' === $title) {
            $values['publication'] = $section->getParagraphs()[0]->getContent();
        }
    }
}
