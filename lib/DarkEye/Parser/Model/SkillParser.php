<?php

namespace DarkEye\Parser\Model;

use DarkEye\Parser\AbstractPageParser;
use DarkEye\Parser\Content\Page;
use DarkEye\Parser\Content\Paragraph;
use DarkEye\Parser\Content\Section;
use DarkEye\Parser\PageProvider\NestedDirectoryParser;
use DarkEye\Schema\Skill;
use Symfony\Component\Serializer\Serializer;

final class SkillParser extends AbstractPageParser
{
    use PagesContainingAnimalsTrait;

    /**
     * @return string
     */
    public function getModelClass()
    {
        return Skill::class;
    }

    /**
     * @param string $treeDir
     *
     * @return NestedDirectoryParser
     */
    public function getPageProvider($treeDir, Serializer $serializer)
    {
        return new NestedDirectoryParser($treeDir, $this->getSourceDirectories(), $serializer);
    }

    /**
     * @return string[]
     */
    protected function getSourceDirectories(): array
    {
        return [
            'de/sonderfertigkeiten',
            'de/wege-der-vereinigungen-ab-18-jahre/wege-der-vereinigungen-regelteile/sex-sonderfertigkeiten',
        ];
    }

    /**
     * @param \DarkEye\Parser\Content\Section[] $sections
     *
     * @return \DarkEye\Parser\Content\Section[]
     */
    protected function fixSections(array $sections, Page $page): array
    {
        $sections = $this->fixPassiveTag($sections, $page);
        $sections = $this->fixTraditions($sections, $page);
        $sections = $this->fixAnimalSection($sections, $page);
        $sections = $this->fixSchoenheit($sections, $page);
        $sections = $this->fixMeisterlicheGeschossAbwehr($sections, $page);

        return $this->addSkillType($sections, $page);
    }

    /**
     * @param Section[] $sections
     *
     * @return Section[]
     */
    private function fixTraditions($sections, Page $page): array
    {
        if (0 !== \strpos($page->getName(), 'tradition-')) {
            return $sections;
        }

        if (empty($sections)) {
            return $sections;
        }
        $section = $sections[0];
        $extractedParagraphs = $section->extractParagraphs('#^Voraussetzungen\:#');

        $valid = [];
        $invalid = [];
        foreach ($extractedParagraphs as $paragraph) {
            if ($paragraph->mayHaveLabel()) {
                $valid[] = $paragraph;
            } else {
                $invalid[] = $paragraph;
            }
        }
        foreach ($invalid as $paragraph) {
            $section->addParagraph($paragraph);
        }
        $html = $section->joinParagraphs();
        $section->setParagraphs($valid);
        $section->addParagraph(new Paragraph('Regel: '.$html));

        return $sections;
    }

    /**
     * @param Section[] $sections
     *
     * @return \DarkEye\Parser\Content\Section[]
     */
    private function fixSchoenheit(array $sections, Page $page): array
    {
        if ('schoenheit-der-dar-klajid-i-iii' !== $page->getName()) {
            return $sections;
        }

        $notes = $sections[0]->extractParagraphs('@^\\*\\) Zur Erinnerung@');
        foreach ($notes as $note) {
            $sections[0]->appendToParagraphThatStartsWith('Wirkung:', $note->getContent());
        }

        return $sections;
    }

    /**
     * @param Section[] $sections
     *
     * @return \DarkEye\Parser\Content\Section[]
     */
    private function fixMeisterlicheGeschossAbwehr(array $sections, Page $page): array
    {
        if ('meisterliche-geschossabwehr-i-iii' !== $page->getName()) {
            return $sections;
        }

        $notes = $sections[0]->extractParagraphs('@^Modifikatoren für die Raufen-PA:@', '@^#@');
        foreach ($notes as $note) {
            $sections[0]->appendToParagraphThatStartsWith('Regel:', $note->getContent());
        }

        return $sections;
    }

    /**
     * @param Section[] $sections
     *
     * @return Section[]
     */
    private function addSkillType($sections, Page $page): array
    {
        $breadcrumb = $page->getBreadcrumb();

        $segments = $breadcrumb->getSegments();
        array_pop($segments);
        if ('sonderfertigkeiten' === $segments[0]->getSlug()) {
            $type = $segments[1]->getTitle();

            $subPath = array_slice($segments, 2);
            $subType = implode(' &gt; ', $subPath);
        } else {
            $subType = array_pop($segments)->getTitle();
            $type = array_pop($segments)->getTitle();
        }

        if (0 === count($sections)) {
            $sections[0] = new Section($page->getTitle());
        }

        $type = str_replace('Sonderfertigkeiten', 'SF', $type);
        $subType = str_replace(['Sonderfertigkeiten', 'Sonderfertigkeit', 'sonderfertigkeiten'], ['SF', 'SF', '-SF'], $subType);

        $sections[0]->addParagraph(new Paragraph('skill_type: '.$type));
        $sections[0]->addParagraph(new Paragraph('sub_type: '.$subType));

        return $sections;
    }

    /**
     * @param Section[] $sections
     *
     * @return \DarkEye\Parser\Content\Section[]
     */
    private function fixPassiveTag($sections, Page $page): array
    {
        if (0 === count($sections)) {
            return $sections;
        }

        $paragraph = $sections[0]->getParagraphs()[0];
        if ('(passiv)' === $paragraph->getContent()) {
            $paragraph->replace('#^\(passiv\)$#', 'is_passive: passiv');
        }

        return $sections;
    }
}
