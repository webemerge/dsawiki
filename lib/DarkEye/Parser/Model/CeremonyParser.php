<?php

namespace DarkEye\Parser\Model;

use DarkEye\Parser\AbstractPageParser;
use DarkEye\Parser\Content\Page;
use DarkEye\Parser\Content\Section;
use DarkEye\Schema\Ceremony;

final class CeremonyParser extends AbstractPageParser
{
    use PagesContainingAnimalsTrait;

    /**
     * @return string
     */
    public function getModelClass()
    {
        return Ceremony::class;
    }

    /**
     * @return string[]
     */
    protected function getSourceDirectories(): array
    {
        return [
            'de/goetterwirken/zeremonien',
        ];
    }

    /**
     * @param Section[] $sections
     *
     * @return \DarkEye\Parser\Content\Section[]
     */
    protected function fixSections(array $sections, Page $page)
    {
        $sections = $this->fixInlineExtensions($sections);
        $sections = $this->fixAnimalSection($sections, $page);

        return $this->fixPublicationSection($sections);
    }

    /**
     * @param \DarkEye\Parser\Content\Section[] $sections
     *
     * @return array
     */
    private function fixInlineExtensions($sections)
    {
        if (count($sections) < 1) {
            return $sections;
        }

        foreach ($sections as $section) {
            $extracted = $section->extractParagraphs('@^Liturgieerweiterungen:$@');
            if (count($extracted) > 0) {
                unset($extracted[0]);
                $sections[] = new Section('Liturgieerweiterungen', $extracted);
            }
        }

        return $sections;
    }

    // protected function detectSections(Crawler $crawler)
    // {
    //     $sections = parent::detectSections($crawler);
    //     $sections = $this->fixZwergwalDescription($sections);
    //     $sections = $this->fixCeremonyDuration($sections);
    //     $sections = $this->fixGestalt($sections, 'Löwengestalt', 'Löwe');
    //     $sections = $this->fixInlineExtensions($sections);
    //     $sections = $this->fixGestalt($sections, 'Delphingestalt', 'Delphin');
    //     $sections = $this->fixKorsmal($sections);
    //
    //     return $sections;
    // }

    private function fixCeremonyDuration($sections)
    {
        foreach ($sections as $s => $section) {
            foreach ($section['paragraphs'] as $p => $paragraph) {
                $sections[$s]['paragraphs'][$p] = str_replace('Zeremoniendauer', 'Zeremoniedauer', $paragraph);
            }
        }

        return $sections;
    }

    private function fixZwergwalDescription($sections)
    {
        if (!isset($sections[0]) || 'Zwergwalgestalt' !== $sections[0]['name']) {
            return $sections;
        }

        $description = $sections[0]['paragraphs'][0];
        $effect = $sections[0]['paragraphs'][2];
        $sections[0]['paragraphs'][2] = preg_replace('#\: #', ': '.$description.'<br><br>', $effect, 1);
        unset($sections[0]['paragraphs'][0]);
        $sections[0]['paragraphs'] = array_values($sections[0]['paragraphs']);

        return $sections;
    }

    private function fixGestalt($sections, $ceremonyName, $animal)
    {
        if (!isset($sections[0]) || $sections[0]['name'] !== $ceremonyName) {
            return $sections;
        }

        $newParagraphs = [];
        $started = false;
        foreach ($sections[0]['paragraphs'] as $paragraph) {
            // Löwengestalt contains a lion description that can be merged into the description paragraph
            if ($started && preg_match('@^Publikation\:@', $paragraph)) {
                $started = false;
            }

            if ($started) {
                $newParagraphs[1] .= "<br>\n".$paragraph;

                continue;
            }
            if ('<strong>'.$animal.'</strong>' === $paragraph) {
                $started = true;
                $newParagraphs[1] .= "<br>\n".$paragraph;

                continue;
            }

            $newParagraphs[] = $paragraph;
        }

        $sections[0]['paragraphs'] = $newParagraphs;

        return $sections;
    }

    private function fixKorsmal($sections)
    {
        if (!isset($sections[0]) || 'Empfängnis des Korsmals' !== $sections[0]['name']) {
            return $sections;
        }

        $newParagraphs = [];
        $started = false;
        foreach ($sections[0]['paragraphs'] as $paragraph) {
            // Empfängnis des Korsmals contains a variation description that can be merged into the description paragraph
            if ($started && preg_match('@^Publikation\:@', $paragraph)) {
                $started = false;
            }

            if ($started) {
                $newParagraphs[1] .= "<br>\n".$paragraph;

                continue;
            }
            if ('Varianten:' === $paragraph) {
                $started = true;
                $newParagraphs[1] .= "<br>\n".$paragraph;

                continue;
            }

            $newParagraphs[] = $paragraph;
        }

        $sections[0]['paragraphs'] = $newParagraphs;

        return $sections;
    }

    /**
     * Override depending on model.
     */
    protected function mapFurtherSection(Section $section, array &$values, int $index, Page $page)
    {
    }

    /**
     * Override depending on model.
     *
     * @param array $section
     * @param array $values
     * @param int   $index
     */
    protected function __mapFurtherSection($section, &$values, $index)
    {
        if (1 !== $index) {
            $this->logger->error('Found more than 2 sections:');
            $this->dump($section, $values);
        }

        if ('Liturgieerweiterungen' !== $section['name'] && 'Liturgieerweiterungen:' !== $section['name']) {
            $this->logger->error('Unknown section name: '.$section['name']);
            $this->exit($section, $values);
        }

        foreach ($section['paragraphs'] as $paragraph) {
            if (preg_match('@^• <em>(.*) </em>\((.*)\)\: (.*)@', $paragraph, $matches)) {
                $values['ceremony_extensions'][] = [
                    'name' => $matches[1],
                    'requirements' => $matches[2],
                    'description' => $matches[3],
                ];
            } elseif (preg_match('@^<em> #(.*) </em>\((.*)\)\: (.*)@', $paragraph, $matches)) {
                $values['ceremony_extensions'][] = [
                    'name' => $matches[1],
                    'requirements' => $matches[2],
                    'description' => $matches[3],
                ];
            } elseif (preg_match('@^<em>#(.*) </em>\((.*)\)\: (.*)@', $paragraph, $matches)) {
                $values['ceremony_extensions'][] = [
                    'name' => $matches[1],
                    'requirements' => $matches[2],
                    'description' => $matches[3],
                ];
            } elseif (preg_match('@^Publikation\: (.*)@', $paragraph, $matches)) {
                $itemsCount = count($values['ceremony_extensions']);
                for ($i = 0; $i < $itemsCount; ++$i) {
                    $values['ceremony_extensions'][$i]['publication'] = $matches[1];
                }
            } elseif (preg_match('@(^<em>[^#]+.*)@', $paragraph, $matches)) {
                $values['ceremony_extensions'][count($values['ceremony_extensions']) - 1]['description'] .= ' '.$matches[1];
            } elseif (preg_match('@(^der .*)@', $paragraph, $matches)) {
                $values['ceremony_extensions'][count($values['ceremony_extensions']) - 1]['description'] .= ' '.$matches[1];
            } elseif (preg_match('@(^können geweiht .*)@', $paragraph, $matches)) {
                $values['ceremony_extensions'][count($values['ceremony_extensions']) - 1]['description'] .= ' '.$matches[1];
            } else {
                $this->logger->error('Unknown ceremony extension paragraph: '.$paragraph);
                $this->dump($section, $values);
                die();
            }
        }
    }
}
