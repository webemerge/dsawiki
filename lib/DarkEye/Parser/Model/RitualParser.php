<?php

namespace DarkEye\Parser\Model;

use DarkEye\Schema\Ritual;

final class RitualParser extends MagicSpellParser
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Ritual::class;
    }

    /**
     * @return string[]
     */
    protected function getSourceDirectories(): array
    {
        return [
            'de/magie/rituale',
        ];
    }
}
