<?php

namespace DarkEye\Parser\Model;

use DarkEye\Parser\Content\Page;

trait PagesContainingAnimalsTrait
{
    /**
     * @param \DarkEye\Parser\Content\Section[] $sections
     */
    private function fixAnimalSection(array $sections, Page $page): array
    {
        $pagesWithSeparateAnimalSection = [
            'affenruf' => 2,
            // 'eulenruf' => 3,
            'gefaehrte-in-neuer-gestalt' => 1,
        ];
        $pagesWithAnimalInMainSection = [
            // 'delphingestalt' => 'Delphin',
            'halluzinationsgift' => 'Halluzinationsgift',
            // 'schlangenruf' => 'Hexenschlangen',
            'seilschlange' => 'Seilschlange',
            'tierwandlung' => 'Chamäleon (mittlere Tierverwandlung, Echsenpfad)',
            'giftdruesen' => 'Mishkaragift',
        ];

        if (
            !isset($pagesWithSeparateAnimalSection[$page->getName()]) &&
            !isset($pagesWithAnimalInMainSection[$page->getName()])
        ) {
            return $sections;
        }

        $this->logger->debug('Fixing animal section');
        if (isset($pagesWithAnimalInMainSection[$page->getName()])) {
            $animalName = $pagesWithAnimalInMainSection[$page->getName()];
            $animal = $sections[0]->extractParagraphs(
                '@^'.preg_quote($animalName, '@').'$@',
                '@^Publikation@'
            );

            if (!isset($animal[0])) {
                $this->logger->error("Cannot find animal paragraph: $animalName");
                $this->exit($animal, $page);
            }
            $animal[0] = '<h3>'.$animal[0].'</h3>';
            $animal = implode("<br>\n", $animal);
        } else {
            $sectionNumber = $pagesWithSeparateAnimalSection[$page->getName()];
            $animal = $sections[$sectionNumber]->toHtml();
            unset($sections[$sectionNumber]);
        }

        $sections[0]->appendToParagraphThatStartsWith('Wirkung:', $animal);

        return array_values($sections);
    }
}
