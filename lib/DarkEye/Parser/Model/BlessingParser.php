<?php

namespace DarkEye\Parser\Model;

use DarkEye\Parser\AbstractPageParser;
use DarkEye\Schema\Blessing;

final class BlessingParser extends AbstractPageParser
{
    use PagesContainingPrimaryEffectTrait;

    /**
     * @return string
     */
    public function getModelClass()
    {
        return Blessing::class;
    }

    /**
     * @return string[]
     */
    protected function getSourceDirectories(): array
    {
        return [
            'de/goetterwirken/segnungen',
        ];
    }
}
