<?php

namespace DarkEye\Parser\Model;

use DarkEye\Parser\AbstractPageParser;
use DarkEye\Parser\Content\Page;
use DarkEye\Parser\Content\Section;
use DarkEye\Schema\Liturgy;

final class LiturgyParser extends AbstractPageParser
{
    use PagesContainingGenericDescriptionsAndSectionsTrait;

    /**
     * @return string
     */
    public function getModelClass()
    {
        return Liturgy::class;
    }

    /**
     * @return string[]
     */
    protected function getSourceDirectories(): array
    {
        return [
            'de/goetterwirken/liturgien',
        ];
    }

    /**
     * @param \DarkEye\Parser\Content\Section[] $sections
     *
     * @return Section[]
     */
    protected function fixSections(array $sections, Page $page)
    {
        $sections = $this->fixDuplicateSection($sections, $page);
        $sections = $this->fixInlineExtensions($sections);
        $sections = $this->fixSchlangenstab($sections);
        $sections = $this->fixWindruf($sections);
        $sections = $this->fixSchleichendeFaeulnisPflanzen($sections);

        return $this->mergeAdditionalSectionsIntoFirstSection($sections, $page);
    }

    /**
     * @param Section[] $sections
     *
     * @return \DarkEye\Parser\Content\Section[]
     */
    private function fixDuplicateSection($sections, Page $page): array
    {
        if (isset($sections[2]) && isset($sections[1]) && $sections[1]->getParagraphs() === $sections[2]->getParagraphs()) {
            unset($sections[2]);
        }

        return $sections;
    }

    /**
     * @param \DarkEye\Parser\Content\Section[] $sections
     *
     * @return Section[]
     */
    private function fixInlineExtensions($sections)
    {
        if (count($sections) < 1) {
            return $sections;
        }

        foreach ($sections as $section) {
            $extracted = $section->extractParagraphs('@^Liturgieerweiterungen$@');
            if (count($extracted) > 0) {
                unset($extracted[0]);
                $sections[] = new Section('Liturgieerweiterungen', $extracted);
            }
        }
        foreach ($sections as $section) {
            $extracted = $section->extractParagraphs('@^Liturgieerweiterungen:$@');
            if (count($extracted) > 0) {
                unset($extracted[0]);
                $sections[] = new Section('Liturgieerweiterungen', $extracted);
            }
        }

        return $sections;
    }

    /**
     * @param \DarkEye\Parser\Content\Section[] $sections
     *
     * @return \DarkEye\Parser\Content\Section[]
     */
    private function fixSchlangenstab($sections)
    {
        if ('Schlangenstab' !== $sections[0]->getTitle()) {
            return $sections;
        }

        $extracted = $sections[0]->extractParagraphs('@^Stabschlange$@');
        $html = '<h3>'.$extracted[0].'</h3>';
        unset($extracted[0]);
        $html .= implode("<br>\n", $extracted);
        $html .= $sections[1]->toHtml();

        $sections[0]->getParagraphs()[1]->append("<br>\n");

        unset($sections[1]);

        return array_values($sections);
    }

    /**
     * @param Section[] $sections
     *
     * @return \DarkEye\Parser\Content\Section[]
     */
    private function fixWindruf($sections)
    {
        if ('Windruf' !== $sections[0]->getTitle()) {
            return $sections;
        }

        $extracted = $sections[0]->extractParagraphs('@^# <em>@');
        $sections[] = new Section('Liturgieerweiterungen', $extracted);

        return $sections;
    }

    /**
     * @param \DarkEye\Parser\Content\Section[] $sections
     *
     * @return Section[]
     */
    private function fixSchleichendeFaeulnisPflanzen(array $sections)
    {
        if ('Schleichende Fäulnis (Pflanzen)' !== $sections[0]->getTitle()) {
            return $sections;
        }

        foreach ($sections[0]->getParagraphs() as $paragraph) {
            $paragraph->replace('@^S<strong>teigerungsfaktor:</strong>@', 'Steigerungsfaktor:');
        }

        return $sections;
    }

    /**
     * Override depending on model.
     */
    protected function mapFurtherSection(Section $section, array &$values, int $index, Page $page)
    {
        $publications = $section->extractParagraphs('@^Publikation:$@');
        if (!empty($publications)) {
            $publications = implode(', ', $publications);
            if (isset($values['publication'])) {
                $values['publication'] .= ', '.$publications;
            } else {
                $values['publication'] = $publications;
            }
        }

        if ('Publikationen' === $section->getTitle() || 'Publikation' === $section->getTitle()) {
            $values['publication'] = implode(', ', $section->getParagraphs());

            return;
        }

        if (!$section->hasContent()) {
            return;
        }

        // if (1 !== $index) {
        //     dump('Found more than 2 sections:');
        //     dump($section);
        //     dump($values);
        // }

        if ('Liturgieerweiterungen' !== $section->getTitle() && 'Liturgieerweiterungen:' !== $section->getTitle()) {
            dump('Unknown section name: '.$section->getTitle());
            dump($section);
            dump($values);
            die();
        }

        foreach ($section->getParagraphs() as $paragraph) {
            $paragraph = preg_replace('@^<strong>(.*)</strong>$@', '\1', $paragraph);
            $paragraph = str_replace(['<em>', '</em>'], '', $paragraph);
            $currentExt = isset($values['liturgy_extensions']) ? count($values['liturgy_extensions']) - 1 : 0;
            if (preg_match('@^• (.*) \((.*)\)\: (.*)@', $paragraph, $matches)) {
                $values['liturgy_extensions'][] = [
                    'name' => $matches[1],
                    'requirements' => $matches[2],
                    'description' => $matches[3],
                ];
            } elseif (preg_match('@^#[ ]?(.*) \((.*)\)\: (.*)@', $paragraph, $matches)) {
                $values['liturgy_extensions'][] = [
                    'name' => $matches[1],
                    'requirements' => $matches[2],
                    'description' => $matches[3],
                ];
            } elseif (preg_match('@^(\S*) \((.*)\)\: (.*)@', $paragraph, $matches)) {
                $values['liturgy_extensions'][] = [
                    'name' => $matches[1],
                    'requirements' => $matches[2],
                    'description' => $matches[3],
                ];
            } elseif (preg_match('@^Publikation\: (.*)@', $paragraph, $matches)) {
                $itemsCount = count($values['liturgy_extensions']);
                for ($i = 0; $i < $itemsCount; ++$i) {
                    $values['liturgy_extensions'][$i]['publication'] = $matches[1];
                }
            } elseif (preg_match('@(^<em>[^#]+.*)@', $paragraph, $matches)) {
                $values['liturgy_extensions'][$currentExt]['description'] .= ' '.$matches[1];
            } elseif (preg_match('@(^der .*)@', $paragraph, $matches)) {
                $values['liturgy_extensions'][$currentExt]['description'] .= ' '.$matches[1];
            } elseif (preg_match('@^(<strong>umfasst.*)@', $paragraph, $matches)) {
                $values['liturgy_extensions'][$currentExt]['description'] .= ' '.$matches[1];
            } elseif (preg_match('@^(<strong>doch.*)@', $paragraph, $matches)) {
                $values['liturgy_extensions'][$currentExt]['description'] .= ' '.$matches[1];
            } elseif (preg_match('@(^können geweiht .*)@', $paragraph, $matches)) {
                $values['liturgy_extensions'][$currentExt]['description'] .= ' '.$matches[1];
            } elseif (preg_match('@(^gerichtet ist.*)@', $paragraph, $matches)) {
                $values['liturgy_extensions'][$currentExt]['description'] .= ' '.$matches[1];
            } elseif (preg_match('@(^Die Wirkungsdauer.*)@', $paragraph, $matches)) {
                $values['liturgy_extensions'][$currentExt]['description'] .= ' '.$matches[1];
            } elseif (preg_match('@(^umfasst auch.*)@', $paragraph, $matches)) {
                $values['liturgy_extensions'][$currentExt]['description'] .= ' '.$matches[1];
            } elseif (preg_match('@(^aus. Der Schaden .*)@', $paragraph, $matches)) {
                $values['liturgy_extensions'][$currentExt]['description'] .= ' '.$matches[1];
            } elseif (preg_match('@(^hierbei .*)@', $paragraph, $matches)) {
                $values['liturgy_extensions'][$currentExt]['description'] .= ' '.$matches[1];
            } elseif (preg_match('@(^doch modifizier.*)@', $paragraph, $matches)) {
                $values['liturgy_extensions'][$currentExt]['description'] .= ' '.$matches[1];
            } elseif (preg_match('@(^beträgt.*)@', $paragraph, $matches)) {
                $values['liturgy_extensions'][$currentExt]['description'] .= ' '.$matches[1];
            } elseif (preg_match('@^Errata ([\d]{2}\.[\d]{2}\.[\d]{4}):(.*)@', $paragraph, $matches)) {
                $text = $matches[1].': '.$matches[2];
                $values['errata'] = isset($values['errata']) ? $values['errata']."<br>\n".$text : $text;
            } elseif (preg_match('@^• <em>Zielkategorie</em>$@', $paragraph, $matches)) {
                // drop
            } elseif (preg_match('@^S$@', $paragraph, $matches)) {
                // drop
            } elseif (preg_match('@^P/TP anrichten,$@', $paragraph, $matches)) {
                // drop
            } else {
                dump('Unknown liturgy extension paragraph: '.$paragraph);
                dump($section);
                dump($values);
                die();
            }
        }
    }
}
