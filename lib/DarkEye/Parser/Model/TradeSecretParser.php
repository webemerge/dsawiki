<?php

namespace DarkEye\Parser\Model;

use DarkEye\Parser\AbstractTableParser;
use DarkEye\Schema\TradeSecret;

final class TradeSecretParser extends AbstractTableParser
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return TradeSecret::class;
    }

    /**
     * @return string
     */
    protected function getPageFilename()
    {
        return 'SF_Berufsgeheimnisse.html';
    }

    protected function handleSingleRowValues($row, array $section)
    {
        $row['name'] = $row['trade_secret'];
        unset($row['trade_secret']);
        $row['group'] = $section['name'];

        return $row;
    }
}
