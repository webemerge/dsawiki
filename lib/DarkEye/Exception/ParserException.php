<?php

namespace DarkEye\Exception;

use RuntimeException;

final class ParserException extends RuntimeException
{
    /**
     * @param string $className
     *
     * @return static
     */
    public static function modelParserDoesNotExist($className)
    {
        return new static(sprintf('There is no model parser for model class %s', $className));
    }

    /**
     * @param string $className
     *
     * @return static
     */
    public static function modelClassDoesNotExist($className)
    {
        return new static(sprintf('There is no model class %s', $className));
    }

    /**
     * @param string $filename
     *
     * @return static
     */
    public static function fileNotFound($filename)
    {
        return new static(sprintf('File not found: %s', $filename));
    }
}
