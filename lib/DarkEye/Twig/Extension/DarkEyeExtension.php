<?php

namespace DarkEye\Twig\Extension;

use DarkEye\Entity\Entity;
use DarkEye\Entity\EntityRepository;
use DarkEye\Schema\AbstractSchema;
use DarkEye\Schema\SchemaProvider;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

final class DarkEyeExtension extends AbstractExtension
{
    /**
     * @var EntityRepository
     */
    private $repository;

    /**
     * @var SchemaProvider
     */
    private $schemaProvider;

    public function __construct(EntityRepository $repository, SchemaProvider $schemaProvider)
    {
        $this->repository = $repository;
        $this->schemaProvider = $schemaProvider;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('get_entities', [$this, 'findEntitiesForModel']),
            new TwigFunction('get_schemas', [$this, 'getSchemas']),
        ];
    }

    /**
     * @param $modelName
     *
     * @return Entity[]
     */
    public function findEntitiesForModel($modelName)
    {
        $modelName = $this->schemaProvider->getSchema($modelName)->getName();

        return $this->repository->find($modelName);
    }

    /**
     * @return AbstractSchema[]
     */
    public function getSchemas()
    {
        return $this->schemaProvider->getAll();
    }
}
