<?php

namespace DarkEye\FileTree;

use Cocur\Slugify\SlugifyInterface;
use DarkEye\Parser\Content\Breadcrumb;
use DarkEye\Parser\Content\Page;
use DarkEye\Parser\Content\Paragraph;
use DarkEye\Parser\Content\PathSegment;
use DarkEye\Parser\Content\Section;
use Gajus\Dindent\Indenter;
use HTMLPurifier;
use Psr\Log\LoggerInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

final class FileTreeBuilder
{
    const TERM_PATTERN = '[\w\s\+\-äöüß/\d\(\)]+';

    /**
     * @var string
     */
    private $sourceDir;

    /**
     * @var string
     */
    private $treeDir;

    /**
     * @var HTMLPurifier
     */
    private $purifier;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SlugifyInterface
     */
    private $slugify;

    /**
     * @var \DarkEye\Parser\Content\Page[]
     */
    private $pages;

    /**
     * @var Filesystem
     */
    private $fs;

    /**
     * @var Serializer
     */
    private $serializer;

    public function __construct(
        string $sourceDir,
        string $treeDir,
        HTMLPurifier $purifier,
        LoggerInterface $logger,
        SlugifyInterface $slugify,
        SerializerInterface $serializer
    ) {
        $this->sourceDir = $sourceDir;
        $this->treeDir = $treeDir;
        $this->purifier = $purifier;
        $this->logger = $logger;
        $this->slugify = $slugify;
        $this->serializer = $serializer;

        $this->fs = new Filesystem();
    }

    public function removeTree()
    {
        $this->fs->remove($this->treeDir);
        $this->fs->mkdir($this->treeDir);
    }

    public function buildTree(?string $namePattern = '*.html'): void
    {
        $this->pages = [];
        $finder = Finder::create()
            ->files()
            ->name($namePattern)
            ->notName('start.html')
            ->sortByName()
            ->in($this->sourceDir)
        ;

        foreach ($finder as $file) {
            $this->createPageFromFile($file);
        }
    }

    private function createPageFromFile(SplFileInfo $file): void
    {
        $this->logger->info('Processing '.$file->getFilename());
        $crawler = new Crawler($file->getContents());

        $lang = $this->detectPageLanguage($crawler);
        $breadcrumb = $this->detectPageBreadcrumb($crawler, $file);

        $content = $this->cleanContent($crawler);
        // if (null === $content) {
        //     $this->logger->warning('  Page '.$breadcrumb->getSlug().' is empty');
        //     return;
        // }
        $sections = $this->detectSections($content);

        $page = new Page($breadcrumb, $lang, $content, $file->getFilename(), $sections);

        if (isset($this->pages[$page->getPath()])) {
            dump($this->pages[$page->getPath()]);
            throw new \RuntimeException('Duplicate path: '.$page->getPath());
        }
        $this->pages[$page->getPath()] = $page;

        $this->writePage($page);
    }

    /**
     * @param $crawler
     *
     * @return string
     */
    private function detectPageLanguage(Crawler $crawler)
    {
        $langLink = $crawler->filter('#logo_wrapper .mod_t4c_megamenu > a')->eq(1)->text(null, true);

        return ('EN' === $langLink) ? 'de' : 'en';
    }

    private function detectPageBreadcrumb(Crawler $crawler, SplFileInfo $file): Breadcrumb
    {
        $breadcrumb = new Breadcrumb();
        foreach ($crawler->filter('#sub_header .breadcrumb_boxed ul > li > a') as $node) {
            $link = str_replace('index.php/', '', $node->getAttribute('href'));
            $title = $node->textContent;
            $slug = $this->slugify->slugify($title);
            if ('dsa-regel-wiki' === $slug || 'tde-game-reference' === $slug) {
                continue;
            }

            $segment = new PathSegment($slug, $title, $link);
            $breadcrumb->add($segment);
        }
        $node = $crawler->filter('#sub_header .breadcrumb_boxed ul > li.active.last')->eq(0);
        $title = $node->text(null, true);
        $slug = $this->slugify->slugify($title);
        $segment = new PathSegment($slug, $title, $file->getFilename());
        $breadcrumb->add($segment);

        return $breadcrumb;
    }

    private function writePage(Page $page)
    {
        $this->logger->debug('Writing page '.$page->getPath());

        $path = $this->treeDir.'/'.$page->getPath();
        $this->fs->mkdir($path);
        $meta = $this->serializer->normalize($page->getMetadata());
        $name = $page->getName();
        $sections = $this->serializer->normalize($page->getSections());
        $pageData = $this->serializer->normalize($page);

        file_put_contents("$path/raw.$name.html", $page->getCleanedContent());
        file_put_contents("$path/raw.$name-meta.json", $this->toJson($meta));
        file_put_contents("$path/raw.$name-sections.json", $this->toJson($sections));
        file_put_contents("$path/raw.$name-page.json", $this->toJson($pageData));
    }

    private function toJson(array $data): string
    {
        return json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }

    private function cleanContent(Crawler $crawler)
    {
        try {
            $html = $crawler->filter('#main > .mod_article.first.last.container')->html();
        } catch (\InvalidArgumentException $e) {
            //return null;
            $html = '';
        }

        $html = str_replace(' ', ' ', $html);
        $html = str_replace("\xC2\xA0", '', $html);
        $html = str_replace("\xEF\xBB\xBF", ' ', $html);
        $html = str_replace('–', '-', $html);
        $html = str_replace('<span lang="en-us" xml:lang="en-us">', '<span>', $html);
        $html = str_replace('<span class="fontstyle0">', '<span>', $html);
        $html = str_replace('<span class="fontstyle1">', '<span>', $html);
        $html = str_replace('<span class="fontstyle2">', '<span>', $html);
        $html = str_replace('<span class="fontstyle3">', '<span>', $html);
        $html = str_replace('<span class="fontstyle4">', '<span>', $html);
        $html = str_replace('</strong>:<strong>', ':', $html);
        $html = str_replace(', <br>', ', ', $html);
        $html = str_replace('Publikation(en)', 'Publikation', $html);

        // this will break the HTML at some points but be fixed by the purifier
        $html = str_replace('<br />', '</p><p>', $html);
        $html = str_replace('<br>', '</p><p>', $html);

        $purified = $this->purifier->purify($html);

        $purified = str_replace(' </strong>', '</strong> ', $purified);
        $purified = str_replace('<strong> </strong>', ' ', $purified);
        $purified = str_replace(' </em>', '</em> ', $purified);
        $purified = str_replace('<em> </em>', ' ', $purified);
        $purified = preg_replace('#</ul>\s*<ul>#', '', $purified);

        $purified = preg_replace('#<strong>([^<]+)</p>#', '<strong>\1</strong></p>', $purified);
        $purified = preg_replace('#<p>\s*</strong>#', '<p>', $purified);

        $purified = preg_replace('#\s*</p>#', '</p>', $purified);

        $purified = $this->wrapHtml($purified);
        $content = $this->formatHtml($purified);

        $content = preg_replace('#<p>\s*#im', '<p>', $content);
        $content = preg_replace('#<p><strong>('.self::TERM_PATTERN.'):</strong></p>#i', '</div><div class="ce_text block"><h1>\1</h1>', $content);
        $content = preg_replace('#<p><strong>('.self::TERM_PATTERN.'):</strong> #im', '<p>\1: ', $content);
        $content = preg_replace('#<p><strong>('.self::TERM_PATTERN.')</strong>: #im', '<p>\1: ', $content);

        $content = preg_replace('#<p><strong>(.*)</strong></p>#m', '<p>\1</p>', $content);

        return $this->formatHtml($content);
    }

    public function formatHtml($html): string
    {
        $indenter = new Indenter();

        return $indenter->indent($html);
    }

    /**
     * @param $purified
     */
    private function wrapHtml(string $purified): string
    {
        return <<<HTML
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        {$purified}
    </body>
</html>
HTML;
    }

    private function detectSections(string $content)
    {
        $crawler = new Crawler($content);

        $sections = $crawler->filter('.ce_text.block, .ce_table.block')->each(function (Crawler $sectionNode, $i) {
            return $this->extractSection($sectionNode, $i);
        });

        return array_filter($sections, function (Section $section) {
            return $section->hasContent();
        });
    }

    private function extractSection(Crawler $sectionNode, int $i): Section
    {
        $autoGenerated = false;
        if ($sectionNode->filter('h1, h2, h3')->count() > 0) {
            $title = trim($sectionNode->filter('h1, h2, h3')->eq(0)->text(null, true));
        } else {
            $title = 'Autogenerated '.$i;
            $autoGenerated = true;
        }

        $sectionContainsTable = $sectionNode->filter('.ulTable')->count() > 0;
        if (0 === $i && $sectionContainsTable) {
            $paragraphs = $this->extractFirstTableSectionAsParagraphs($sectionNode, $i);
        } elseif ($sectionContainsTable && !$autoGenerated) {
            $paragraphs = [new Paragraph($this->extractTableSectionContent($sectionNode))];
        } else {
            $paragraphs = $sectionNode->children('p, ul, ol')->each(function (Crawler $paraNode, $i) {
                return new Paragraph($this->extractParagraphNodeHtml($paraNode));
            });
        }

        $paragraphs = array_filter($paragraphs, function (Paragraph $paragraph) {
            return !empty(trim($paragraph->getContent()));
        });

        return new Section($title, $paragraphs);
    }

    private function extractParagraphNodeHtml(Crawler $node): string
    {
        switch ($node->nodeName()) {
            case 'ul':
            case 'ol':
                $html = $node->outerHtml();
                break;

            default:
                $html = $node->html();
        }

        $html = trim($html);
        $html = preg_replace('#^<br>#', '', $html);
        $html = preg_replace('#^<strong>(.*)</strong>$#', '\1', $html);
        $html = preg_replace('#^<strong>(.*)</strong>#', '\1', $html);
        $html = preg_replace('#^<b>(.*)</b>#', '\1', $html);

        return preg_replace('#<strong>[\s]*</strong>#m', '', $html);
    }

    /**
     * @return Paragraph[]
     */
    private function extractFirstTableSectionAsParagraphs(Crawler $sectionNode, int $nodeIndex): array
    {
        $ths = $sectionNode->filter('.ulTable > thead > tr > th')->each(function (Crawler $paraNode, $i) {
            return $this->extractParagraphNodeHtml($paraNode);
        });

        $tds = $sectionNode->filter('.ulTable > tbody > tr > td')->each(function (Crawler $paraNode, $i) {
            return $this->extractParagraphNodeHtml($paraNode);
        });
        $paragraphs = [];

        foreach ($ths as $i => $th) {
            if (empty($th) || !isset($tds[$i])) {
                continue;
            }
            $paragraphs[] = new Paragraph($th.': '.$tds[$i]);
        }

        return $paragraphs;
    }

    private function extractTableSectionContent(Crawler $sectionNode): string
    {
        return $sectionNode->filter('.ulTable')->outerHtml();
    }
}
