<?php

namespace DarkEye\Entity;

final class Entity
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var array
     */
    public $data;

    /**
     * Entity constructor.
     *
     * @param string $id
     * @param string $name
     */
    public function __construct($id, $name, array $data)
    {
        $this->id = $id;
        $this->name = $name;
        $this->data = $data;
    }
}
