<?php

namespace DarkEye\Entity;

use DarkEye\Schema\SchemaProvider;
use Symfony\Component\Filesystem\Filesystem;

final class EntityRepository
{
    /**
     * @var string
     */
    private $dataDir;

    /**
     * @var SchemaProvider
     */
    private $schemaProvider;

    public function __construct(string $dataDir, SchemaProvider $schemaProvider)
    {
        $this->dataDir = $dataDir;
        $this->schemaProvider = $schemaProvider;
    }

    /**
     * @return Entity[]
     */
    public function find(string $modelName): array
    {
        $schema = $this->schemaProvider->getSchema($modelName);
        if (null === $schema) {
            throw new \InvalidArgumentException('Unknown schema: '.$modelName);
        }

        $filename = $this->dataDir.'/'.$schema->getName().'.json';
        $fs = new Filesystem();
        if (!$fs->exists($filename)) {
            return [];
        }

        $data = json_decode(file_get_contents($filename), $assoc = true);

        return $this->hydrate($data);
    }

    /**
     * @return Entity[]
     */
    private function hydrate(array $data): array
    {
        return array_map(function ($entityData) {
            return new Entity($entityData['id'], $entityData['name'], $entityData['data']);
        }, $data);
    }
}
