<?php

namespace DarkEye\Schema;

final class MagicTrick extends AbstractSchema
{
    /**
     * Get all available fields.
     *
     * @return string[]
     */
    public function getFieldNames(): array
    {
        return [
            'name',
            'effect',
            'note',
            'reach',
            'effective_duration',
            'target_category',
            'trait',
            'publication',
        ];
    }

    public function getDefaultTableFieldNames(): array
    {
        return [
            'name',
            // 'effect',
            // 'note',
            'reach',
            'effective_duration',
            'target_category',
            'trait',
            'publication',
        ];
    }

    public function getAlias(): string
    {
        return 'zaubertricks';
    }

    public function getIcon(): string
    {
        return 'fad fa-hand-holding-magic';
    }
}
