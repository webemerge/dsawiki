<?php

namespace DarkEye\Schema;

final class Creature extends AbstractSchema
{
    /**
     * Get all available fields.
     *
     * @return string[]
     */
    public function getFieldNames(): array
    {
        return [
            'name',
            'creature_type',
            'creature_sub_type',
            'creature_type_long',
            'size_category',
            'size',
            'actions',
            'description',
            'publication',
        ];
    }

    public function getDefaultTableFieldNames(): array
    {
        return [
            'name',
            'creature_type',
            'creature_sub_type',
            'size_category',
            'actions',
            'publication',
        ];
    }

    public function getAlias(): string
    {
        return 'bestiarium';
    }

    public function getIcon(): string
    {
        return 'fad fa-cat';
    }

    public function getSubSchemaFieldName(): ?string
    {
        return 'creature_type';
    }

    public function getSubSchemaIcons(): array
    {
        return [
            'Chimären' => 'fa-alicorn',
            'Dämonen' => 'fa-pastafarianism',
            'Daimonid' => 'fa-eye-evil',
            'Drachen' => 'fa-dragon',
            'Elementare' => 'fa-fire',
            'Feen und Feenartige' => 'fa-angel',
            'Geister' => 'fa-ghost',
            'Golems' => 'fa-robot',
            'Homunculi' => 'fa-check-o-lantern',
            'Kulturschaffende' => 'fa-child',
            'Pflanzen' => 'fa-seedling',
            'Tiere' => 'fa-cow',
            'Untote' => 'fa-tombstone',
            'Übernatürliche Wesen' => 'fa-spider-black-widow',
        ];
    }
}
