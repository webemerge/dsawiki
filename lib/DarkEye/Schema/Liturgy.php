<?php

namespace DarkEye\Schema;

final class Liturgy extends AbstractSchema
{
    /**
     * Get all available fields.
     *
     * @return string[]
     */
    public function getFieldNames(): array
    {
        return [
            'name',
            'test_for',
            'effect',
            'liturgy_duration',
            'kap_cost',
            // 'cost_modifiable',
            'reach',
            'effective_duration',
            'target_category',
            'circulation',
            'raise_factor',
            'publication',
            'liturgy_extensions',
            'extended_liturgy_skills',
        ];
    }

    public function getDefaultTableFieldNames(): array
    {
        return [
            'name',
            'test_for',
            // 'effect',
            'liturgy_duration',
            'kap_cost',
            // 'cost_modifiable',
            'reach',
            'effective_duration',
            'target_category',
            'circulation',
            // 'raise_factor',
            'publication',
        ];
    }

    public function getAlias(): string
    {
        return 'liturgien';
    }

    public function getIcon(): string
    {
        return 'fad fa-bible';
    }
}
