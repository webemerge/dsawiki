<?php

namespace DarkEye\Schema;

final class Ceremony extends AbstractSchema
{
    /**
     * Get all available fields.
     *
     * @return string[]
     */
    public function getFieldNames(): array
    {
        return [
            'name',
            'test_for',
            'effect',
            'ceremony_duration',
            'kap_cost',
            // 'cost_modifiable',
            'reach',
            'effective_duration',
            'target_category',
            // 'trait',
            'circulation',
            'raise_factor',
            'publication',
            'ceremony_extensions',
        ];
    }

    public function getDefaultTableFieldNames(): array
    {
        return [
            'name',
            'test_for',
            // 'effect',
            'ceremony_duration',
            'kap_cost',
            // 'cost_modifiable',
            'reach',
            'effective_duration',
            'target_category',
            // 'trait',
            'circulation',
            // 'raise_factor',
            'publication',
        ];
    }

    public function getAlias(): string
    {
        return 'zeremonien';
    }

    public function getIcon(): string
    {
        return 'fad fa-pray';
    }
}
