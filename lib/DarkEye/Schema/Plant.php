<?php

namespace DarkEye\Schema;

final class Plant extends AbstractSchema
{
    /**
     * Get all available fields.
     *
     * @return string[]
     */
    public function getFieldNames(): array
    {
        return [
            'name',
            'search_modifier',
            'identification_modifier',
            'number_of_uses',
            'effect',
            'price',
            'description',
            'publication',
        ];
    }

    public function getDefaultTableFieldNames(): array
    {
        return [
            'name',
            'search_modifier',
            'number_of_uses',
            'price',
            'publication',
        ];
    }

    public function getAlias(): string
    {
        return 'herbarium';
    }

    public function getIcon(): string
    {
        return 'fad fa-leaf';
    }
}
