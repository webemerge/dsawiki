<?php

namespace DarkEye\Schema;

abstract class AbstractSchema
{
    /**
     * @var string
     */
    protected $translatedName;

    /**
     * Get all available field names.
     *
     * @return string[]
     */
    abstract public function getFieldNames(): array;

    public function getName(): string
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    public function getAlias(): string
    {
        return $this->getName();
    }

    public function getDefaultTableFieldNames(): array
    {
        return $this->getFieldNames();
    }

    public function getIcon(): string
    {
        return 'fad fa-circle-o';
    }

    public function getTranslatedName(): string
    {
        return $this->translatedName;
    }

    public function setTranslatedName(string $translatedName): void
    {
        $this->translatedName = $translatedName;
    }

    public function getSubSchemaFieldName(): ?string
    {
        return null;
    }

    public function getSubSchemaIcons(): array
    {
        return [];
    }
}
