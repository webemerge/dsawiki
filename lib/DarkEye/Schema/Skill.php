<?php

namespace DarkEye\Schema;

final class Skill extends AbstractSchema
{
    /**
     * Get all available fields.
     *
     * @return string[]
     */
    public function getFieldNames(): array
    {
        return [
            'name',
            'skill_type',
            'sub_type',
            'is_passive',
            'fighting_styles',
            'extended_fighting_skills',
            'extended_magic_skills',
            'extended_magic_style_skills',
            'extended_liturgy_skills',
            'extended_talent_skills',
            'rule',
            'effect',
            'requirements',
            'difficulty',
            'ap_cost',
            'binding_volume',
            'binding_ap_cost',
            'banning_circle',
            'protection_circle',
            'talent',
            'trait',
            'animal_types',
            'duration',
            'brew',
            'music_tradition',
            'usable_by',
            'errata_box',
            'publication',
        ];
    }

    public function getDefaultTableFieldNames(): array
    {
        return [
            'name',
            'skill_type',
            'sub_type',
            // 'is_passive',
            'fighting_styles',
            // 'extended_fighting_skills',
            // 'extended_magic_skills'
            // 'extended_liturgy_skills',
            // 'extended_talent_skills',
            // 'rule',
            // 'effect',
            // 'requirements',
            'difficulty',
            'ap_cost',
            'binding_volume',
            // 'binding_ap_cost',
            // 'banning_circle',
            // 'protection_circle',
            // 'talent',
            // 'trait',
            // 'animal_types',
            // 'duration',
            // 'brew',
            // 'music_tradition',
            // 'usable_by',
            // 'errata_box',
            'publication',
        ];
    }

    public function getAlias(): string
    {
        return 'sonderfertigkeiten';
    }

    public function getIcon(): string
    {
        return 'fad fa-books';
    }

    public function getSubSchemaFieldName(): ?string
    {
        return 'skill_type';
    }

    public function getSubSchemaIcons(): array
    {
        return [
            'Karmale SF' => 'fa-bible',
            'Magische SF' => 'fa-book-spells',
            'Profane SF' => 'fa-atlas',
            'Sex-SF' => 'fa-book-heart',
            'Tierische SF' => 'fa-monkey',
        ];
    }
}
