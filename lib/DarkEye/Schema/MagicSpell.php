<?php

namespace DarkEye\Schema;

final class MagicSpell extends AbstractSchema
{
    /**
     * Get all available fields.
     *
     * @return string[]
     */
    public function getFieldNames(): array
    {
        return [
            'name',
            'spell_type',
            'test_for',
            'effect',
            'spell_duration',
            'asp_cost',
            // 'cost_modifiable',
            'reach',
            'effective_duration',
            'target_category',
            'trait',
            'circulation',
            'raise_factor',
            'note',
            'publication',
            'spell_extensions',
        ];
    }

    public function getDefaultTableFieldNames(): array
    {
        return [
            'name',
            'spell_type',
            'test_for',
            // 'effect',
            'spell_duration',
            'asp_cost',
            // 'cost_modifiable',
            'reach',
            'effective_duration',
            // 'target_category',
            'trait',
            'circulation',
            // 'raise_factor',
            // 'note',
            'publication',
            // 'spell_extensions',
        ];
    }

    public function getAlias(): string
    {
        return 'magische-handlungen';
    }

    public function getIcon(): string
    {
        return 'fad fa-book-spells';
    }

    public function getSubSchemaFieldName(): ?string
    {
        return 'spell_type';
    }

    public function getSubSchemaIcons(): array
    {
        return [
            'Animistenkräfte' => 'fa-paw-claws',
            'Elfenlieder' => 'fa-microphone-stand',
            'Geodenrituale' => 'fa-hammer-war',
            'Herrschaftsrituale' => 'fa-thunderstorm',
            'Hexenflüche' => 'fa-cauldron',
            'Schelmenstreiche' => 'fa-grin-squint-tears',
            'Verzerrte Elfenlieder' => 'fa-microphone-stand fa-rotate-180',
            'Zaubermelodien' => 'fa-music',
            'Zaubersprüche' => 'fa-wand-magic',
            'Zaubertänze' => 'fa-shoe-prints',
            'Zibiljarituale' => 'fa-scroll-old',
        ];
    }
}
