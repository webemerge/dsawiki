<?php

namespace DarkEye\Schema;

final class Language extends AbstractSchema
{
    /**
     * Get all available fields.
     *
     * @return string[]
     */
    public function getFieldNames(): array
    {
        return [
            'name',
            'group',
            'language_variations',
            'alphabet',
            'assigned_language',
            'upgrade_cost',
            'note',
        ];
    }

    public function getDefaultTableFieldNames(): array
    {
        return [
            'name',
            'group',
            'language_variations',
            'alphabet',
            'assigned_language',
            'upgrade_cost',
            // 'note',
        ];
    }

    public function getAlias(): string
    {
        return 'sprachen';
    }

    public function getIcon(): string
    {
        return 'fad fa-comments';
    }
}
