<?php

namespace DarkEye\Schema;

final class Disadvantage extends AbstractSchema
{
    /**
     * Get all available fields.
     *
     * @return string[]
     */
    public function getFieldNames(): array
    {
        return [
            'name',
            'disadvantage_type',
            'rule',
            'requirements',
            'ap_cost',
            'publication',
            'disadvantage_examples',
        ];
    }

    public function getDefaultTableFieldNames(): array
    {
        return [
            'name',
            'disadvantage_type',
            // 'rule',
            // 'requirements',
            'ap_cost',
            'publication',
            // 'disadvantage_examples',
        ];
    }

    public function getAlias(): string
    {
        return 'nachteile';
    }

    public function getIcon(): string
    {
        return 'fad fa-minus-circle';
    }
}
