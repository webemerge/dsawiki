<?php

namespace DarkEye\Schema;

final class Armor extends AbstractSchema
{
    /**
     * Get all available fields.
     *
     * @return string[]
     */
    public function getFieldNames(): array
    {
        return [
            'name',
            'armor_type',
            'armor_level',
            'burden_level',
            'additional_drain',
            'weight',
            'price',
            'complexity',
            //'rule',
            'armor_advantage',
            'armor_disadvantage',
            'note',
            'publication',
        ];
    }

    public function getDefaultTableFieldNames(): array
    {
        return [
            'name',
            'armor_type',
            'armor_level',
            'burden_level',
            'additional_drain',
            'weight',
            'price',
            'complexity',
            // 'rule',
            // 'armor_advantage',
            // 'armor_disadvantage',
            // 'note',
            // 'publication',
        ];
    }

    public function getAlias(): string
    {
        return 'ruestungen';
    }

    public function getIcon(): string
    {
        return 'fad fa-tshirt';
    }

    public function getSubSchemaFieldName(): ?string
    {
        return 'armor_type';
    }

    public function getSubSchemaIcons(): array
    {
        return [
            'Gestechrüstung' => 'fa-helmet-battle',
            'Holzrüstung' => 'fa-trees',
            'Kettenrüstung' => 'fa-link',
            'Knochenrüstung' => 'fa-skeleton',
            'Lederrüstung' => 'fa-cow',
            'Normale Kleidung/Felle/Nackt' => 'fa-tshirt',
            'Plattenrüstung' => 'fa-hockey-mask',
            'Schuppenrüstung' => 'fa-dragon',
            'Schwere Kleidung/Winterkleidung' => 'fa-mitten',
            'Stoffrüstung' => 'fa-user-tie',
            'Turnierrüstung' => 'fa-helmet-battle',
        ];
    }
}
