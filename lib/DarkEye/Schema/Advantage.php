<?php

namespace DarkEye\Schema;

final class Advantage extends AbstractSchema
{
    /**
     * Get all available fields.
     *
     * @return string[]
     */
    public function getFieldNames(): array
    {
        return [
            'name',
            'advantage_type',
            'rule',
            'requirements',
            'ap_cost',
            'publication',
        ];
    }

    public function getDefaultTableFieldNames(): array
    {
        return [
            'name',
            'advantage_type',
            'ap_cost',
            'publication',
        ];
    }

    public function getAlias(): string
    {
        return 'vorteile';
    }

    public function getIcon(): string
    {
        return 'fad fa-plus-circle';
    }
}
