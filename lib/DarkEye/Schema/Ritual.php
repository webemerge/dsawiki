<?php

namespace DarkEye\Schema;

final class Ritual extends AbstractSchema
{
    /**
     * Get all available fields.
     *
     * @return string[]
     */
    public function getFieldNames(): array
    {
        return [
            'name',
            'test_for',
            'effect',
            'ritual_duration',
            'asp_cost',
            // 'cost_modifiable',
            'reach',
            'effective_duration',
            'target_category',
            'trait',
            'circulation',
            'raise_factor',
            'publication',
        ];
    }

    public function getDefaultTableFieldNames(): array
    {
        return [
            'name',
            'test_for',
            // 'effect',
            'ritual_duration',
            'asp_cost',
            // 'cost_modifiable',
            'reach',
            'effective_duration',
            'target_category',
            'trait',
            'circulation',
            // 'raise_factor',
            'publication',
        ];
    }

    public function getAlias(): string
    {
        return 'rituale';
    }

    public function getIcon(): string
    {
        return 'fad fa-sparkles';
    }
}
