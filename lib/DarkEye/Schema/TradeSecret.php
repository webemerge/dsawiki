<?php

namespace DarkEye\Schema;

final class TradeSecret extends AbstractSchema
{
    /**
     * Get all available fields.
     *
     * @return string[]
     */
    public function getFieldNames(): array
    {
        return [
            'name',
            'group',
            'requirements',
            'ap_cost',
            'publication',
        ];
    }

    public function getDefaultTableFieldNames(): array
    {
        return [
            'name',
            'group',
            'requirements',
            'ap_cost',
            'publication',
        ];
    }

    public function getAlias(): string
    {
        return 'berufsgeheimnisse';
    }

    public function getIcon(): string
    {
        return 'fad fa-user-secret';
    }
}
