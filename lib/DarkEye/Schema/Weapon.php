<?php

namespace DarkEye\Schema;

final class Weapon extends AbstractSchema
{
    /**
     * Get all available fields.
     *
     * @return string[]
     */
    public function getFieldNames(): array
    {
        return [
            'name',
            'weapon_type',
            'is_improvised',
            'damage',
            'main_attribute_damage_treshold',
            'attack_parry_modifiers',
            'loading_time',
            'reach',
            'ammunition',
            'weight',
            'length',
            'price',
            'complexity',
            'rule',
            'weapon_advantage',
            'weapon_disadvantage',
            'note',
            'publication',
        ];
    }

    public function getDefaultTableFieldNames(): array
    {
        return [
            'name',
            'weapon_type',
            'is_improvised',
            'damage',
            'main_attribute_damage_treshold',
            'attack_parry_modifiers',
            'loading_time',
            'reach',
            'ammunition',
            'weight',
            'length',
            'price',
            'complexity',
            // 'rule',
            // 'weapon_advantage',
            // 'weapon_disadvantage',
            // 'note',
            // 'publication',
        ];
    }

    public function getAlias(): string
    {
        return 'waffen';
    }

    public function getIcon(): string
    {
        return 'fad fa-swords';
    }

    public function getSubSchemaFieldName(): ?string
    {
        return 'weapon_type';
    }

    public function getSubSchemaIcons(): array
    {
        return [
            'Armbrüste' => 'fa-bullseye-arrow',
            'Blasrohre' => 'fa-flute',
            'Bögen' => 'fa-bow-arrow',
            'Diskusse' => 'fa-compact-disc',
            'Dolche' => 'fa-dagger',
            'Fächer' => 'fa-galaxy',
            'Fechtwaffen' => 'fa-sword',
            'Hiebwaffen' => 'fa-mace',
            'Holzwaffen' => 'fa-trees',
            'Kettenwaffen' => 'fa-link',
            'Lanzen' => 'fa-horse-saddle',
            'Peitschen' => 'fa-skull-cow',
            'Raufen' => 'fa-hand-rock',
            'Schilde' => 'fa-shield-cross',
            'Schleudern' => 'fa-ball-pile',
            'Schwerter' => 'fa-sword',
            'Spießwaffen' => 'fa-wand',
            'Stangenwaffen' => 'fa-scythe',
            'Wurfwaffen' => 'fa-fan',
            'Zweihandhiebwaffen' => 'fa-axe-battle',
            'Zweihandschwerter' => 'fa-sword',
        ];
    }
}
