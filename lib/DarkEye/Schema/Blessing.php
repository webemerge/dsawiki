<?php

namespace DarkEye\Schema;

final class Blessing extends AbstractSchema
{
    /**
     * Get all available fields.
     *
     * @return string[]
     */
    public function getFieldNames(): array
    {
        return [
            'name',
            'effect',
            'reach',
            'effective_duration',
            'target_category',
            'aspect',
            'publication',
        ];
    }

    public function getDefaultTableFieldNames(): array
    {
        return [
            'name',
            // 'effect',
            'reach',
            'effective_duration',
            'target_category',
            'aspect',
            'publication',
        ];
    }

    public function getAlias(): string
    {
        return 'segnungen';
    }

    public function getIcon(): string
    {
        return 'fad fa-hand-paper';
    }
}
