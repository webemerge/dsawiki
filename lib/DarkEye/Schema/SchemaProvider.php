<?php

namespace DarkEye\Schema;

use Symfony\Component\Finder\Finder;
use Symfony\Contracts\Translation\TranslatorInterface;

final class SchemaProvider
{
    /**
     * @var AbstractSchema[]
     */
    private $schemas = [];

    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
        $this->init();
    }

    private function init(): void
    {
        $finder = new Finder();
        $finder
            ->in(__DIR__)
            ->name('*.php')
        ;

        $this->schemas = [];
        foreach ($finder as $file) {
            $schemaName = basename($file, '.php');
            $className = __NAMESPACE__.'\\'.$schemaName;
            $ref = new \ReflectionClass($className);
            if (!$ref->isAbstract() && $ref->isSubclassOf(AbstractSchema::class)) {
                /* @var $schema AbstractSchema */
                $schema = new $className();
                $translatedName = $this->translator->trans('schema_name.'.$schemaName);
                $schema->setTranslatedName($translatedName);

                $this->schemas[$schemaName] = $schema;
            }
        }

        uasort($this->schemas, function (AbstractSchema $a, AbstractSchema $b) {
            return strcasecmp($a->getTranslatedName(), $b->getTranslatedName());
        });
    }

    /**
     * Get a schema by providing either the schema name, schema FQCN or alias.
     */
    public function getSchema(string $nameOrClassOrAlias): ?AbstractSchema
    {
        if (isset($this->schemas[$nameOrClassOrAlias])) {
            return $this->schemas[$nameOrClassOrAlias];
        }

        foreach ($this->schemas as $schema) {
            if ($schema->getAlias() === $nameOrClassOrAlias || get_class($schema) === $nameOrClassOrAlias) {
                return $schema;
            }
        }

        return null;
    }

    /**
     * @return AbstractSchema[]
     */
    public function getAll(): array
    {
        return $this->schemas;
    }
}
